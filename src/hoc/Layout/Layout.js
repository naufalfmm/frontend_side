import React, { Component } from 'react';

import Header from '../../containers/Header/Header';
import SideDrawers from '../../containers/SideDrawer/SideDrawer';

import styles from './Layout.styles';
import { withStyles } from '../../../node_modules/@material-ui/core';

class Layout extends Component {
    state = {
        openDrawers: false,
        openMenu: false,
        anchorEl: null
    }

    toggleDrawerHandler = (boolState) => {
        this.setState(prevState => ({
            openDrawers: !prevState.openDrawers
        }));
    }

    menuHandler = (event) => {
        console.log("menuHandler", event.currentTarget)
        if (this.state.anchorEl === null)
            this.setState({anchorEl: event.currentTarget})
        else
            this.setState({anchorEl: null})
    }

    logoutHandler = () => {
        this.props.history.replace('/logout')
    }

    render () {
        const { classes } = this.props
        console.log("Layout", this.state.openMenu, this.state.anchorEl, this.props)
        return (
            <div>
                <Header toggleDrawer={this.toggleDrawerHandler} toggleMenu={(event) => this.menuHandler(event)} logoutHandler={this.logoutHandler} anchorEl={this.state.anchorEl} />
                <SideDrawers openDrawer={this.state.openDrawers} toggleDrawer={this.toggleDrawerHandler} />
                <div className={classes.content}>
                   {this.props.children}
                </div>
            </div>
        )
    }
}

export default withStyles(styles)(Layout)