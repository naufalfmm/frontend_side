import React, { Component } from 'react';
import Auxi from '../Auxi/Auxi';
import { Snackbar } from '../../../node_modules/@material-ui/core';

const withErrorHandler = (WrappedComponent, axios, text) => {
    return class extends Component {
        state = {
            ret: null
        }

        componentWillMount() {
            console.log("withErrorHandler willMount", text)
            this.resInterceptor = axios.interceptors.response.use(res => {
                console.log("withErrorHandler success response", text, res, this.state, res.response)
                if (res.data.message !== undefined) {
                    this.setState({ ret: res.data })
                }
                return res
            }, error => {
                console.log("withErrorHandler error response", text, error, this.state, error.response)
                if (error.response !== undefined) {
                    console.log("error response data not undefined")
                    this.setState({ ret: error.response.data })
                } else {
                    console.log("error response data undefined")
                    this.setState({ ret: error })
                }
            })
        }

        componentWillUnmount() {
            console.log("withErrorHandler willUnmount", text)
            // console.log('Will Unmount', this.reqInterceptor, this.resInterceptor)
            // axios.interceptors.request.eject(this.reqInterceptor);
            axios.interceptors.response.eject(this.resInterceptor);
        }

        errorConfirmedHandler = () => {
            this.setState({ ret: null })
        }

        render() {
            console.log("withErrorHandler render", this.state, text)

            // let message = null
            // if (this.state.ret !== null) {
            //     if (this.state.ret.data !== undefined) {
            //         message = this.state.ret.data.message
            //     } else {
            //         message = this.state.ret.message
            //     }
            // }

            return (
                <Auxi>
                    <Snackbar
                        anchorOrigin={{
                            vertical: 'top',
                            horizontal: 'center',
                        }}
                        open={this.state.ret !== null}
                        autoHideDuration={6000}
                        onClose={this.errorConfirmedHandler}
                        ContentProps={{
                            'aria-describedby': 'message-id',
                        }}
                        message={<span id="message-id">{this.state.ret ? this.state.ret.message : null}</span>}
                    />
                    <WrappedComponent {...this.props} />
                </Auxi>
            );
        }
    }
}

export default withErrorHandler;