import grey from '@material-ui/core/colors/grey';
import lime from '@material-ui/core/colors/lime';

const styles = theme => ({
    dialogContent: {
        marginTop: theme.spacing.unit * 3
    },
    contentForm: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    contentSimulation: {
        marginBottom: theme.spacing.unit * 2
    },
    content: {
        flexBasis: '47%',
        flexGrow: 1,
        paddingBottom: theme.spacing.unit * 2,
        '&.right': {
            paddingRight: theme.spacing.unit,
        },
        '&.left': {
            paddingLeft: theme.spacing.unit,
        }
    },
    dialogActions: {
        margin: theme.spacing.unit * 3,
        '& button': {
            marginLeft: theme.spacing.unit,
            marginRight: theme.spacing.unit,
            backgroundColor: lime[900],
            width: 86,
            color: theme.palette.common.white,
            '&:hover': {
                backgroundColor: lime[800]
            },
            '&[disabled]': {
                backgroundColor: grey[600]
            }
        }
    },
    circular: {
        left: '45%',
        position: 'relative',
        color: lime[900]
    }
})

export default styles