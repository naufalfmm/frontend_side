import React, { Component } from 'react';
import axios from '../../../axios-ins';

import Auxi from '../../../hoc/Auxi/Auxi';

import styles from './AddAdmin.styles';
import { CircularProgress, FormControl, FormLabel, TextField, 
        MenuItem, Divider, DialogActions, Button, withStyles, Dialog, DialogTitle, DialogContent } from '@material-ui/core';

class AddAdmin extends Component {
    state = {
        loading: false,
        error: false,
        form: {
            fullname: {
                value: '',
                valid: false,
                touched: false,
                validate: {
                    require: true
                }
            },
            email: {
                value: '',
                valid: false,
                touched: false,
                validate: {
                    require: true,
                    isEmail: true
                }
            },
            username: {
                value: '',
                valid: false,
                touched: false,
                validate: {
                    require: true
                }
            },
            password: {
                value: '',
                valid: false,
                touched: false,
                validate: {
                    require: true
                }
            },
            active: {
                value: 0,
                valid: false,
                touched: false,
                validate: {
                    require: true,
                    isNumber: true,
                }
            },
            grade: {
                value: 0,
                valid: false,
                touched: false,
                validate: {
                    require: true,
                    isNumber: true,
                }
            },
        }
    }

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.require) {
            isValid = value.toString().trim() !== '' && isValid;
        }

        if (rules.isNumber) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isEmail) {
            const pattern = /[\w-]+@([\w-]+\.)+[\w-]+/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.min) {
            isValid = value >= rules.min && isValid
        }

        if (rules.max) {
            isValid = value >= rules.max && isValid
        }

        return isValid;
    }

    changeHandler = (name, event) => {
        event.preventDefault()
        console.log("changeHandler", name)
        let formUpdate = {
            ...this.state.form,
            [name]: {
                ...this.state.form[name],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.form[name].validate),
                touched: true
            }
        }

        this.setState({ form: formUpdate })
    }

    formHandler = () => {
        this.props.history.push('/admin')
    }

    submitHandler = () => {
        this.setState({ loading: true, error: false })

        let addData = {}
        addData["username"] = this.state.form.username.value
        addData["password"] = this.state.form.password.value
        addData["name"] = this.state.form.fullname.value
        addData["email"] = this.state.form.email.value
        addData["grade"] = parseInt(this.state.form.grade.value)
        addData["active"] = parseInt(this.state.form.active.value)

        axios.post('admin/registration', addData)
            .then(() => {
                this.setState({ loading: false })
                this.props.history.push('/admin')
            })
            .catch(() => {
                this.setState({ loading: false, error: true })
            })
    }

    render() {
        const { classes } = this.props

        let form = (
            <Auxi>
                <CircularProgress className={classes.circular} size={60} />
            </Auxi>
        )

        let actions = null

        let disableButton = (
            !(this.state.form.fullname.valid && this.state.form.fullname.touched) ||
            !(this.state.form.email.valid && this.state.form.email.touched) ||
            !(this.state.form.username.valid && this.state.form.username.touched) ||
            !(this.state.form.password.valid && this.state.form.password.touched) ||
            !(this.state.form.active.valid && this.state.form.active.touched) ||
            !(this.state.form.grade.valid && this.state.form.grade.touched)
        )

        if (!this.state.loading) {
            form = (
                <Auxi>
                    <div className={classes.contentForm}>
                        <FormControl required className={[classes.content, "right"].join(' ')}>
                            <FormLabel component="legend">Username</FormLabel>
                            <TextField
                                required
                                error={!this.state.form.username.valid && this.state.form.username.touched}
                                id="username"
                                name="username"
                                value={this.state.form.username.value}
                                onChange={(event) => this.changeHandler('username', event)}
                            />
                        </FormControl>
                        <FormControl required className={[classes.content, "left"].join(' ')}>
                            <FormLabel component="legend">Password</FormLabel>
                            <TextField
                                required
                                error={!this.state.form.password.valid && this.state.form.password.touched}
                                id="password"
                                name="password"
                                type="password"
                                value={this.state.form.password.value}
                                onChange={(event) => this.changeHandler('password', event)}
                            />
                        </FormControl>
                    </div>
                    <div className={classes.contentForm}>
                        <FormControl required className={[classes.content, "right"].join(' ')}>
                            <FormLabel component="legend">Full Name</FormLabel>
                            <TextField
                                required
                                error={!this.state.form.fullname.valid && this.state.form.fullname.touched}
                                id="fullname"
                                name="fullname"
                                value={this.state.form.fullname.value}
                                onChange={(event) => this.changeHandler('fullname', event)}
                            />
                        </FormControl>
                        <FormControl required className={[classes.content, "left"].join(' ')}>
                            <FormLabel component="legend">Email</FormLabel>
                            <TextField
                                required
                                error={!this.state.form.email.valid && this.state.form.email.touched}
                                id="email"
                                name="email"
                                value={this.state.form.email.value}
                                onChange={(event) => this.changeHandler('email', event)}
                            />
                        </FormControl>
                    </div>
                    {/*  */}
                    <div className={classes.contentForm}>
                        <FormControl required className={[classes.content, "right"].join(' ')}>
                            <FormLabel component="legend">Role</FormLabel>
                            <TextField
                                select
                                required
                                error={!this.state.form.grade.valid && this.state.form.grade.touched}
                                id="grade"
                                name="grade"
                                value={this.state.form.grade.value}
                                onChange={(event) => this.changeHandler('grade', event)}
                            >
                                <MenuItem value='' />
                                <MenuItem value={1}>{"Master Admin"}</MenuItem>
                                <MenuItem value={2}>{"Admin"}</MenuItem>
                            </TextField>
                        </FormControl>
                        <FormControl required className={[classes.content, "left"].join(' ')}>
                            <FormLabel component="legend">Active Status</FormLabel>
                            <TextField
                                select
                                required
                                error={!this.state.form.active.valid && this.state.form.active.touched}
                                id="active"
                                name="active"
                                value={this.state.form.active.value}
                                onChange={(event) => this.changeHandler('active', event)}
                            >
                                <MenuItem value='' />
                                <MenuItem value={1}>Active</MenuItem>
                                <MenuItem value={0}>Inactive</MenuItem>
                            </TextField>
                        </FormControl>
                    </div>
                </Auxi>
            )

            actions = (
                <Auxi>
                    <Divider />
                    <DialogActions className={classes.dialogActions}>
                        <Button
                            color="primary"
                            disabled={disableButton}
                            onClick={this.submitHandler}>
                            Add
                        </Button>
                        <Button color="primary" onClick={this.formHandler}>Cancel</Button>
                    </DialogActions>
                </Auxi>
            )
        }

        return (
            <Dialog
                onClose={this.formHandler}
                open
            >
                <DialogTitle>{"Add Admin"}</DialogTitle>
                <Divider />
                <DialogContent className={classes.dialogContent}>
                    {form}
                </DialogContent>
                {actions}
            </Dialog>
        )
    }
}

export default withStyles(styles)(AddAdmin)