import lime from '@material-ui/core/colors/lime';

const styles = theme => ({
    root: {
        width: '100%',
        height: '100%',
    },
    tablePaper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    tablePaperHeader: {
        height: 80,
        '& h2': {
            position: 'relative',
            top: '50%',
            transform: 'translateY(-50%)',
            float: 'left'
        },
        '& button': {
            position: 'relative',
            marginLeft: theme.spacing.unit,
            marginRight: theme.spacing.unit,
            top: '50%',
            transform: 'translateY(-50%)',
            float: 'right',
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            '&:hover': {
                backgroundColor: lime[800]
            }
        }
    },
    tablePaperContent: {
        marginTop: theme.spacing.unit * 2,
        overflow: 'auto'
    },
    tableHeader: {
        '& th': {
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            textAlign: 'center',
            fontSize: '0.8rem'
        }
    },
    tableBody: {
        '& td': {
            textAlign: 'center',
            fontSize: '0.8rem'
        }
    },
    editDeleteIcon: {
        float: 'right',
    },
    expansionPanel: {
        width: '100%'
    },
    filterTextGroup: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        // flexGrow: 2,
        justifyContent: 'space-between',
        width: '90%',
        alignItems: 'center'
        // margin: theme.spacing.unit
    },
    filterTextButton: {
        width: '10%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        // flexWrap: 'wrap',
        // flex: 1,
        // justifyContent: 'space-between',
        marginLeft: theme.spacing.unit * 4,
        '& button': {
            marginTop: theme.spacing.unit,
            marginBottom: theme.spacing.unit,
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            '&:hover': {
                backgroundColor: lime[800]
            }
        }
    },
    dialog: {
        height: 300
    },
    dialogContent: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        justifyContent: 'space-between',
        width: '95%',
        alignItems: 'center',
        alignContent: 'space-between',
        height: 'auto'
    },
    contentOfDialogContent: {
        marginTop: theme.spacing.unit
    },
    radioButtonGroup: {
        display: 'flex',
        flexDirection: 'row'
    }
})

export default styles