import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { Paper, Typography, withStyles, Button, Divider,
        Table, TableHead, TableRow, TableCell, TableBody, 
        IconButton, Checkbox, ExpansionPanel, ExpansionPanelSummary,
        ExpansionPanelDetails, FormControl, FormLabel, TextField, 
        Select, MenuItem, Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';
import styles from './AdminPage.styles';
import axios from '../../axios-ins';

import AddIcon from '@material-ui/icons/Add';
import RefreshIcon from '@material-ui/icons/Refresh';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import SearchIcon from '@material-ui/icons/Search';
import RemoveIcon from '@material-ui/icons/Remove';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import Snackbar from '../../hoc/Snackbar/Snackbar';

import AddAdmin from './AddAdmin/AddAdmin';

class AdminPage extends Component {
    state = {
        filterState: false,
        dataResp: [],
        error: false,
        errorResp: {},
        filter: {
            fullname: '',
            email: '',
            username: '',
            active: ''
        },
        dataFilter: [],
        addForm: {
            open: false,
            fullname: '',
            email: '',
            username: '',
            password: '',
            active: 0
        },
        editForm: {
            open: false,
            fullname: '',
            email: '',
            username: '',
            active: 0
        }
    }

    defaultFilter = {
        fullname: '',
        email: '',
        username: '',
        active: ''
    }

    componentDidMount() {
        console.log("AdminPage didMount", this.props)
        this.unlisten = this.props.history.listen((location, action) => {
            console.log("AdminPage on route change");
            const pattern = /^(\/admin)/
            if (pattern.test(location.pathname)) this.getAllDataHandler()
        });
        this.getAllDataHandler()
    }

    componentWillUnmount() {
        console.log("AdminPage WillUnmount")
        this.unlisten()
    }

    getAllDataHandler = () => {
        console.log("AdminPage start getAllDataHandler")
        this.setState({ error: false, errorResp: {} })
        axios.get('/admin/all')
            .then(res => {
                this.setState({ dataResp: res.data.admin_data })
                console.log("AdminPage finish getAllDataHandler")
            })
            .catch(e => {
                if (e.response) {
                    this.setState({
                        error: true,
                        errorResp: e.response
                    })
                }
            })
    }

    deleteAdminHandler = (admin_id) => {
        this.setState({ error: false, errorResp: {} })
        axios.delete('/admin/delete/' + admin_id)
            .then(() => {
                this.getAllDataHandler()
            })
            .catch(e => {
                if (e.response) {
                    this.setState({
                        error: true,
                        errorResp: e.response
                    })
                }
            })
    }

    addAdminChangeHandler = (name, event) => {
        console.log(name, event.target.value)
        let addFormUpdate = { ...this.state.addForm }
        addFormUpdate[name] = event.target.value
        this.setState({ addForm: addFormUpdate })
    }

    addAdminModalHandler = () => {
        // let addFormUpdate = { ...this.state.addForm }
        // addFormUpdate.open = !this.state.addForm.open
        // this.setState({ addForm: addFormUpdate })
        this.props.history.replace('/admin/add')
    }

    addAdminHandler = () => {
        this.setState({ error: false })
        var addData = {}
        addData["name"] = this.state.addForm.fullname
        addData["email"] = this.state.addForm.email
        addData["username"] = this.state.addForm.username
        addData["password"] = this.state.addForm.password
        addData["active"] = this.state.addForm.active

        axios.post('/admin/registration/', addData)
            .then(() => {
                this.setState({
                    addForm: {
                        open: false,
                        roadName: '',
                        lanes: '',
                        directions: '',
                        population: '',
                        density: ''
                    }
                })
                this.getAllDataHandler()
            })
            .catch(e => {
                if (e.response) {
                    this.setState({
                        error: true,
                        errorResp: e.response
                    })
                }
            })
    }

    editAdminModalHandler = () => {

    }

    editAdminChangeHandler = (name, event) => {

    }

    editAdminHandler = () => {

    }

    changeFilterHandler = (name, event) => {
        let filterUpdate = this.state.filter
        filterUpdate[name] = event.target.value
        this.setState({ filter: filterUpdate })
    }

    filterHandler = () => {
        this.setState({ filterState: true })

        const data = this.state.dataResp.slice()
        const dataShow = []

        for (let i = 0; i < data.length; i++) {
            if (this.state.filter.fullname !== '') {
                console.log('not empty fullname', this.state.filter.fullname)
                let re = new RegExp('^(' + this.state.filter.fullname.toLowerCase() + ')')
                if ((!re.test(data[i].admin_name.toLowerCase()))) continue
            }

            if (this.state.filter.email !== '') {
                let re = new RegExp('^(' + this.state.filter.email.toLowerCase() + ')')
                if ((!re.test(data[i].admin_email.toLowerCase()))) continue
            }

            if (this.state.filter.username !== '') {
                let re = new RegExp('^(' + this.state.filter.username.toLowerCase() + ')')
                if ((!re.test(data[i].admin_username.toLowerCase()))) continue
            }

            console.log(data[i].admin_active, this.state.filter.active, 
                data[i].admin_active === this.state.filter.active, this.state.filter.active !== '')
            if (this.state.filter.active !== '') {
                if (data[i].admin_active !== this.state.filter.active) continue
            }

            dataShow.push(data[i])
        }

        this.setState({ dataFilter: dataShow })
    }

    resetFilterHandler = () => {
        console.log('resetFilter')
        this.setState({
            filterState: false,
            filter: { ...this.defaultFilter },
            dataFilter: []
        })
    }

    changeActiveAdmin = (adminId, status) => {
        this.setState({ error: false, errorResp: {} })
        console.log(adminId, status)
        axios.post('admin/status/' + status + '/' + adminId)
            .then(() => {
                this.getAllDataHandler()
            })
            .catch(e => {
                if (e.response) {
                    this.setState({
                        error: true,
                        errorResp: e.response
                    })
                }
            })
    }

    render () {
        const { classes } = this.props

        let dataShow = this.state.dataResp.slice()
        if (this.state.filterState) {
            dataShow = this.state.dataFilter.slice()
        }

        let tableData = []

        if (dataShow.length > 0) {
            for (let i = 0; i < dataShow.length; i++) {
                tableData.push(
                    <TableRow key={i}>
                        <TableCell style={{ textAlign: 'left' }} >{dataShow[i].admin_name}</TableCell>
                        <TableCell>{dataShow[i].admin_email}</TableCell>
                        <TableCell>{dataShow[i].admin_username}</TableCell>
                        <TableCell>{dataShow[i].admin_grade === 1 ? 'Master Admin' : 'Admin'}</TableCell>
                        <TableCell padding="checkbox">
                            <Checkbox 
                                checked={dataShow[i].admin_active === 1 ? true : false}
                                onChange={() => this.changeActiveAdmin(dataShow[i].admin_id, dataShow[i].admin_active === 1 ? false : true)}
                            />
                        </TableCell>
                        {
                            this.props.adminStatus !== 'admin' ? (
                                <TableCell>
                                    <IconButton className={classes.editDeleteIcon} variant='fab' mini>
                                        <DeleteIcon onClick={() => this.deleteAdminHandler(dataShow[i].admin_id)} />
                                    </IconButton>
                                </TableCell>
                            ) : null
                        }
                        
                    </TableRow>
                )
            }
        }

        return (
            <div className={classes.root}>
                <ExpansionPanel
                    className={classes.expansionPanel}
                >
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography variant="title">
                            Filter Admin Table
                        </Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <div className={classes.filterTextGroup}>
                            <FormControl style={{width: 'auto'}}>
                                <FormLabel component="legend">Full Name</FormLabel>
                                <TextField
                                    id="fullname"
                                    name="fullname"
                                    value={this.state.filter.fullname}
                                    onChange={(event) => this.changeFilterHandler('fullname', event)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel component="legend">Email</FormLabel>
                                <TextField
                                    id="email"
                                    name="email"
                                    value={this.state.filter.email}
                                    onChange={(event) => this.changeFilterHandler('email', event)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel component="legend">Username</FormLabel>
                                <TextField
                                    id="username"
                                    name="username"
                                    value={this.state.filter.username}
                                    onChange={(event) => this.changeFilterHandler('username', event)}
                                />
                            </FormControl>
                            <FormControl>
                                <FormLabel component="legend">Active Status</FormLabel>
                                <Select
                                    name="active"
                                    value={this.state.filter.active}
                                    onChange={(event) => this.changeFilterHandler('active', event)}
                                >
                                    <MenuItem value='' />
                                    <MenuItem value={1}>Active</MenuItem>
                                    <MenuItem value={0}>Inactive</MenuItem>
                                </Select>
                            </FormControl>
                        </div>
                        <div className={classes.filterTextButton}>
                            <Button variant='fab' mini>
                                <SearchIcon onClick={this.filterHandler} />
                            </Button>
                            <Button variant='fab' mini>
                                <RemoveIcon onClick={this.resetFilterHandler} />
                            </Button>
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <Paper className={classes.tablePaper} elevation={1}>
                    <div className={classes.tablePaperHeader}>
                        <Typography variant="title">
                            Admin Data Table
                        </Typography>`
                        <Button variant='fab' mini>
                            <RefreshIcon onClick={this.getAllDataHandler} />
                        </Button>
                        <Button variant='fab' mini>
                            <AddIcon onClick={this.addAdminModalHandler} />
                        </Button>
                    </div>
                    <Divider />
                    <Paper className={classes.tablePaperContent}>
                        <Table>
                            <TableHead className={classes.tableHeader}>
                                <TableRow>
                                    <TableCell>Full Name</TableCell>
                                    <TableCell>Email</TableCell>
                                    <TableCell>Username</TableCell>
                                    <TableCell>Role</TableCell>
                                    <TableCell>Active</TableCell>
                                    {
                                        this.props.adminStatus !== 'admin' ? <TableCell style={{ width: 96 }} >Actions</TableCell> : null
                                    }
                                </TableRow>
                            </TableHead>
                            <TableBody className={classes.tableBody}>
                                {tableData}
                            </TableBody>
                        </Table>
                    </Paper>
                </Paper>

                <Switch>
                    {/* Add Admin */}
                    <Route
                        path={this.props.match.path + '/add'}
                        component={AddAdmin}
                    />
                </Switch>
                {/* <Dialog open={this.state.addForm.open} onClose={this.addAdminModalHandler}>
                    <DialogTitle id="add-admin-form">Add Admin</DialogTitle>
                    <DialogContent>
                        <div className={classes.dialogContent}>
                            <FormControl className={classes.contentOfDialogContent}>
                                <FormLabel component="legend">Full Name</FormLabel>
                                <TextField
                                    id="add-fullname"
                                    name="add-fullname"
                                    value={this.state.addForm.fullname}
                                    onChange={(event) => this.addAdminChangeHandler('fullname', event)}
                                />
                            </FormControl>
                            <FormControl className={classes.contentOfDialogContent}>
                                <FormLabel component="legend">Email</FormLabel>
                                <TextField
                                    id="add-email"
                                    name="add-email"
                                    value={this.state.addForm.email}
                                    onChange={(event) => this.addAdminChangeHandler('email', event)}
                                />
                            </FormControl>
                            <FormControl className={classes.contentOfDialogContent}>
                                <FormLabel component="legend">Username</FormLabel>
                                <TextField
                                    id="add-username"
                                    name="add-username"
                                    value={this.state.addForm.username}
                                    onChange={(event) => this.addAdminChangeHandler('username', event)}
                                />
                            </FormControl>
                            <FormControl className={classes.contentOfDialogContent}>
                                <FormLabel component="legend">Password</FormLabel>
                                <TextField
                                    id="add-password"
                                    name="add-password"
                                    value={this.state.addForm.password}
                                    onChange={(event) => this.addAdminChangeHandler('password', event)}
                                    type="password"
                                />
                            </FormControl>
                            <FormControl className={classes.contentOfDialogContent}>
                                <FormLabel component="legend">Active Status</FormLabel>
                                <Select
                                    name="add-active"
                                    value={this.state.addForm.active}
                                    onChange={(event) => this.addAdminChangeHandler('active', event)}
                                >
                                    <MenuItem value='' />
                                    <MenuItem value={1}>Active</MenuItem>
                                    <MenuItem value={0}>Inactive</MenuItem>
                                </Select>
                            </FormControl>
                        </div>
                    </DialogContent>
                    <Divider />
                    <DialogActions>
                        <Button color="primary" onClick={this.addAdminModalHandler}>Cancel</Button>
                        <Button color="primary" onClick={this.addAdminHandler}>Add</Button>
                    </DialogActions>
                </Dialog> */}

                {/* Edit Admin */}
                <Dialog open={this.state.editForm.open} onClose={this.editAdminModalHandler}>
                    <DialogTitle id="edit-admin-form">Edit Admin</DialogTitle>
                    <DialogContent>
                        <div className={classes.dialogContent}>
                            <FormControl className={classes.contentOfDialogContent}>
                                <FormLabel component="legend">Full Name</FormLabel>
                                <TextField
                                    id="edit-fullname"
                                    name="edit-fullname"
                                    value={this.state.editForm.fullname}
                                    onChange={(event) => this.editAdminChangeHandler('fullname', event)}
                                />
                            </FormControl>
                            <FormControl className={classes.contentOfDialogContent}>
                                <FormLabel component="legend">Email</FormLabel>
                                <TextField
                                    id="edit-email"
                                    name="edit-email"
                                    value={this.state.editForm.email}
                                    onChange={(event) => this.editAdminChangeHandler('email', event)}
                                />
                            </FormControl>
                            <FormControl className={classes.contentOfDialogContent}>
                                <FormLabel component="legend">Username</FormLabel>
                                <TextField
                                    id="edit-username"
                                    name="edit-username"
                                    value={this.state.editForm.username}
                                    onChange={(event) => this.editAdminChangeHandler('username', event)}
                                />
                            </FormControl>
                            <FormControl className={classes.contentOfDialogContent}>
                                <FormLabel component="legend">Active Status</FormLabel>
                                <Select
                                    name="edit-active"
                                    value={this.state.editForm.active}
                                    onChange={(event) => this.editAdminChangeHandler('active', event)}
                                >
                                    <MenuItem value='' />
                                    <MenuItem value={1}>Active</MenuItem>
                                    <MenuItem value={0}>Inactive</MenuItem>
                                </Select>
                            </FormControl>
                        </div>
                    </DialogContent>
                    <Divider />
                    <DialogActions>
                        <Button color="primary" onClick={this.editAdminModalHandler}>Cancel</Button>
                        <Button color="primary" onClick={this.editAdminHandler}>Edit</Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        adminStatus: state.auth.status
    }
}

export default connect(mapStateToProps, null)(withStyles(styles)(AdminPage))