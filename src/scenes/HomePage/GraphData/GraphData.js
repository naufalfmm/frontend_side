import React from 'react';
import { withStyles, Paper, Typography } from '@material-ui/core';
import { Pie, HorizontalBar } from 'react-chartjs-2';
import styles from './GraphData.sytles';

const graphData = (props) => {
    const { classes } = props

    console.log("graphData", props)

    return (
        <div className={classes.gridChart}>
            <div className={classes.gridChartItem}>
                <Paper className={classes.chartPaperText} style={{ backgroundColor: '#F5F5F5' }}>
                    <div>
                        <Typography variant="title" className={classes.chartTitle}>Vehicle Quantities</Typography>
                    </div>
                    <div style={{ height: 252 }} >
                        <Pie
                            height={252}
                            data={props.qtyData}
                            options={props.qtyOpt}
                        />
                    </div>
                </Paper>
            </div>
            <div className={classes.gridChartItem}>
                <Paper className={classes.chartPaperText} style={{ backgroundColor: '#F5F5F5' }}>
                    <div>
                        <Typography variant="title" className={classes.chartTitle}>Vehicle Mean Speed</Typography>
                    </div>
                    <div style={{ height: 252 }} >
                        <HorizontalBar
                            height={252}
                            data={props.speedData}
                            options={props.speedOpt}
                        />
                    </div>
                </Paper>
            </div>
        </div>
    )
}

export default withStyles(styles)(graphData)