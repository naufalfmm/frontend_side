const styles = theme => ({
    gridChart: {
        marginTop: theme.spacing.unit * 2,
        display: 'flex',
        flexWrap: 'wrap'
    },
    gridChartItem: {
        flexGrow: 1,
        flexBasis: '50%',
        minWidth: 560,
        // margin: 8
    },
    chartPaperText: {
        height: 300,
        margin: theme.spacing.unit
    },
    chartTitle: {
        textAlign: 'center',
        padding: theme.spacing.unit
    },
})

export default styles