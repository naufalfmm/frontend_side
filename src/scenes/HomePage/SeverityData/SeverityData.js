import React, { Component } from 'react';
import axios from '../../../axios-ins';
import { withStyles, Paper, TableCell, TableRow, CircularProgress, Table, TableHead, TableBody, Typography, IconButton, Divider } from '@material-ui/core';
import { Pie, HorizontalBar } from 'react-chartjs-2';
import { Redirect } from 'react-router-dom';
import moment from 'moment';

import styles from './SeverityData.styles';

import RefreshIcon from '@material-ui/icons/Refresh';
import CloseIcon from '@material-ui/icons/Close';

import Auxi from '../../../hoc/Auxi/Auxi';

class SeverityData extends Component {
    state = {
        loading: false,
        error: false,
        dataResp: []
    }

    componentDidMount() {
        console.log("SeverityData didMount")
        this.getDataHandler()
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.severityStat !== this.props.match.params.severityStat) {
            console.log("SeverityData didUpdate", prevProps.match.params.severityStat, this.props.match.params.severityStat)
            this.getDataHandler()
        }
    }

    getDataHandler = () => {
        console.log("SeverityData getDataHandler", this.props.match.params.severityStat)
        this.setState({ loading: true })
        axios.get('/data/' + this.props.match.params.severityStat)
            .then(res => {
                console.log("SeverityData getDataHandler", res.data)
                let stateUpdate = {...this.state}
                stateUpdate.loading = false
                stateUpdate.dataResp = res.data.data
                stateUpdate.error = false
                this.setState(stateUpdate)
            })
            .catch(e => {
                let stateUpdate = { ...this.state }
                stateUpdate.loading = false
                stateUpdate.error = true
                stateUpdate.dataResp = []
                this.setState(stateUpdate)
            })
    }

    render() {
        const { classes } = this.props

        console.log("SeverityData render", this.state)

        let form = (
            <Auxi>
                <CircularProgress className={classes.circular} size={60} />
            </Auxi>
        )

        const dataResp = this.state.dataResp.slice()

        console.log(this.state.error)
        if (this.state.error) {
            console.log("error")
            form = (
                <Redirect to="/" />
            )
        }

        if (!this.state.loading && dataResp.length > 0) {
            let table = null
            let tableData = []

            let vehicleCount = { "motorcycle": 0, "lightvehicle": 0, "heavyvehicle": 0 }
            let speedMean = { "motorcycle": 0, "lightvehicle": 0, "heavyvehicle": 0 }

            let mtcQty = 0
            let lvQty = 0
            let hvQty = 0

            for (let i = 0; i < dataResp.length; i++) {
                let segmentsData = {...dataResp[i]}
                console.log("segmentData",segmentsData)
                tableData.push(
                    <TableRow key={segmentsData.segment_id}>
                        <TableCell>{moment(segmentsData.received_time).subtract(15, 'minutes').format("DD-MM-YYYY HH:mm:ss") + " - " + moment(segmentsData.received_time).format("DD-MM-YYYY HH:mm:ss")}</TableCell>
                        <TableCell>{"Jln. " + segmentsData.node.lane.path.segment.road.road_name}</TableCell>
                        <TableCell>{segmentsData.node.lane.path.segment.segment_start + " - " + segmentsData.node.lane.path.segment.segment_end}</TableCell>
                        <TableCell>{segmentsData.node.lane.path.path_th + "/" + segmentsData.node.lane.lane_th}</TableCell>
                        <TableCell>{segmentsData.status}</TableCell>
                    </TableRow>
                )
                vehicleCount.motorcycle += segmentsData.vehicledatum.motorcycle_qty
                vehicleCount.lightvehicle += segmentsData.vehicledatum.lightvehicle_qty
                vehicleCount.heavyvehicle += segmentsData.vehicledatum.heavyvehicle_qty
                speedMean.motorcycle += segmentsData.vehicledatum.motorcycle_meanspeed
                speedMean.lightvehicle += segmentsData.vehicledatum.lightvehicle_meanspeed
                speedMean.heavyvehicle += segmentsData.vehicledatum.heavyvehicle_meanspeed

                if (segmentsData.vehicledatum.motorcycle_qty > 0) mtcQty++
                if (segmentsData.vehicledatum.lightvehicle_qty > 0) lvQty++
                if (segmentsData.vehicledatum.heavyvehicle_qty > 0) hvQty++
            }

            speedMean.motorcycle = speedMean.motorcycle / mtcQty
            speedMean.lightvehicle = speedMean.lightvehicle / lvQty
            speedMean.heavyvehicle = speedMean.heavyvehicle / hvQty

            table = (
                <Table>
                    <TableHead className={classes.tableHeader}>
                        <TableRow>
                            <TableCell>Traffic Time</TableCell>
                            <TableCell>Road Name</TableCell>
                            <TableCell>Segment</TableCell>
                            <TableCell>Path/Lane</TableCell>
                            <TableCell>Status</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody className={classes.tableBody}>
                        {tableData.slice()}
                    </TableBody>
                </Table>
            )

            const labelVehicle = ['Motorcycle', 'Light Vehicle', 'Heavy Vehicle']
            const labelStatus = ['A', 'B', 'C', 'D', 'E', 'F']

            const optPie = {
                maintainAspectRatio: false,
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var dataset = data.datasets[tooltipItem.datasetIndex];
                            var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                return previousValue + currentValue;
                            });
                            var currentValue = dataset.data[tooltipItem.index];
                            var percentage = Math.floor(((currentValue / total) * 100) + 0.5);

                            return data.labels[tooltipItem.index] + ' ' + percentage + "%";
                        }
                    }
                }
            }

            const optBar = {
                maintainAspectRatio: false,
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var dataset = data.datasets[tooltipItem.datasetIndex];
                            var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                                return previousValue + currentValue;
                            });
                            var currentValue = dataset.data[tooltipItem.index];
                            var percentage = Math.floor(((currentValue / total) * 100) + 0.5);

                            return currentValue.toFixed(2);
                        }
                    }
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }

            const dataQtyVeh = {
                labels: labelVehicle,
                datasets: [{
                    label: 'Qty',
                    data: Object.values(vehicleCount),
                    backgroundColor: [
                        "#EC407A",
                        "#5C6BC0",
                        "#26C6DA"
                    ],
                    hoverBackgroundColor: [
                        "#E91E63",
                        "#3F51B5",
                        "#00BCD4"
                    ]
                }]
            }

            const dataSpdVeh = {
                labels: labelVehicle,
                datasets: [{
                    label: 'm/s',
                    data: Object.values(speedMean),
                    backgroundColor: [
                        "#EC407A",
                        "#5C6BC0",
                        "#26C6DA"
                    ],
                    hoverBackgroundColor: [
                        "#E91E63",
                        "#3F51B5",
                        "#00BCD4"
                    ]
                }]
            }

            form = (
                <Auxi>
                    <div className={classes.paperHeader}>
                        <Typography className={classes.paperHeaderText} variant="subheading">{"Severity Data (" + this.props.match.params.severityStat + ")"}</Typography>
                        <div className={classes.paperHeaderActions}>
                            <IconButton mini>
                                <CloseIcon onClick={() => this.props.history.push('/')} />
                            </IconButton>
                            <IconButton mini>
                                <RefreshIcon onClick={this.getDataHandler} />
                            </IconButton>
                        </div>
                    </div>
                    <Divider />
                    <div className={classes.paperContent}>
                        <div className={classes.trafficGraphGroup}>
                            <div className={classes.trafficGraph}>
                                <Paper className={classes.trafficGraphItem} style={{ backgroundColor: '#F5F5F5' }}>
                                    <div>
                                        <Typography variant="title" className={classes.trafficGraphTitle}>Vehicle Quantities</Typography>
                                    </div>
                                    <div style={{ height: 252 }} >
                                        <Pie
                                            height={252}
                                            data={dataQtyVeh}
                                            options={optPie}
                                        />
                                    </div>
                                </Paper>
                            </div>
                            <div className={classes.trafficGraph}>
                                <Paper className={classes.trafficGraphItem} style={{ backgroundColor: '#F5F5F5' }}>
                                    <div>
                                        <Typography variant="title" className={classes.trafficGraphTitle}>Vehicle Mean Speed</Typography>
                                    </div>
                                    <div style={{ height: 252 }} >
                                        <HorizontalBar
                                            height={252}
                                            data={dataSpdVeh}
                                            options={optBar}
                                        />
                                    </div>
                                </Paper>
                            </div>
                        </div>
                        <div className={classes.trafficTable}>
                            {table}
                        </div>
                    </div>
                </Auxi>
            )
        }

        return (
            <Paper className={classes.paper}>
                {form}
            </Paper>
        )
    }
}

export default withStyles(styles)(SeverityData)