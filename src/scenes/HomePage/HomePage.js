import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import axios from '../../axios-ins';
import CountTo from 'react-count-to';
import { withStyles, Grid, Paper, Typography } from '@material-ui/core';
import { Pie, HorizontalBar } from 'react-chartjs-2';

import styles from './HomePage.styles';

import SeverityData from './SeverityData/SeverityData';
import GraphData from './GraphData/GraphData';

import FaceIcon from '@material-ui/icons/Face';
import DeviceIcon from '@material-ui/icons/Devices';
import TrafficIcon from '@material-ui/icons/Traffic';
import ViewStreamIcon from '@material-ui/icons/ViewStream';

import Snackbar from '../../hoc/Snackbar/Snackbar';

class HomePage extends Component {
    state = {
        error: false,
        loading: false,
        dataRoad: [],
    }

    componentDidMount () {
        console.log("HomePage didMount", this.props.match.params, this.props.match.path)
        this.unlisten = this.props.history.listen((location, action) => {
            console.log("HomePage on route change", location.pathname.split("/"));
            const pattern = /^$/
            console.log(pattern.test(location.pathname))
            if (pattern.test(location.pathname)) this.getAllDataHandler()
        });
        this.getAllDataHandler()
    }

    componentWillUnmount() {
        console.log("HomePage WillUnmount")
        this.unlisten()
    }

    getAllDataHandler = () => {
        console.log("HomePage start getAllDataHandler", this.props.match.params.sevStat)
        this.setState({ loading: false, error: false })
        axios.get('/data/severity')
            .then(res => {
                let stateUpdate = {...this.state}
                stateUpdate.loading = false
                stateUpdate.dataRoad = res.data.data
                this.setState(stateUpdate)
                console.log("HomePage finish getAllDataHandler")
            })
            .catch(e => {
                let stateUpdate = { ...this.state }
                stateUpdate.loading = false
                stateUpdate.dataRoad = []
                stateUpdate.error = false
                this.setState(stateUpdate)
            })
    }

    render () {
        const { classes } = this.props

        const trafficData = this.state.dataRoad.slice()

        console.log("HomePage render", this.props.match.path, trafficData)
        // const adminData = this.state.dataAdmin.slice()
        // let nodeCount = 0
        // let trafficCount = 0
        // let roadCount = trafficData.length

        let status = { "A": 0, "B": 0, "C": 0, "D": 0, "E": 0, "F": 0 }
        let vehicleCount = { "motorcycle": 0, "lightvehicle": 0, "heavyvehicle": 0 }
        let speedMean = { "motorcycle": 0, "lightvehicle": 0, "heavyvehicle": 0 }

        let mtcQty = 0
        let lvQty = 0
        let hvQty = 0

        if (trafficData.length > 0) {
            for (let i = 0; i < trafficData.length; i++) {
                status[trafficData[i].status] += 1
                console.log("trafficData", trafficData[i])
                vehicleCount.motorcycle += trafficData[i].vehicledatum.motorcycle_qty
                vehicleCount.lightvehicle += trafficData[i].vehicledatum.lightvehicle_qty
                vehicleCount.heavyvehicle += trafficData[i].vehicledatum.heavyvehicle_qty
                speedMean.motorcycle += trafficData[i].vehicledatum.motorcycle_meanspeed
                speedMean.lightvehicle += trafficData[i].vehicledatum.lightvehicle_meanspeed
                speedMean.heavyvehicle += trafficData[i].vehicledatum.heavyvehicle_meanspeed
                if (trafficData[i].vehicledatum.motorcycle_qty > 0) mtcQty++
                if (trafficData[i].vehicledatum.lightvehicle_qty > 0) lvQty++
                if (trafficData[i].vehicledatum.heavyvehicle_qty > 0) hvQty++
            }
        }

        speedMean.motorcycle = speedMean.motorcycle / mtcQty
        speedMean.lightvehicle = speedMean.lightvehicle / lvQty
        speedMean.heavyvehicle = speedMean.heavyvehicle / hvQty

        // for (let i = 0; i < trafficData.length; i++) {
        //     let lanesData = trafficData[i].lanes.slice()
        //     nodeCount += lanesData.length
        //     for (let j = 0; j < lanesData.length; j++) {
        //         let severityData = lanesData[j].node.severitystatuses.slice()
        //         trafficCount += severityData.length
        //         for (let k = 0; k < severityData.length; k++) {
        //             status[severityData[k].status] += 1
        //             vehicleCount.motorcycle += severityData[k].vehicledatum.motorcycle_qty
        //             vehicleCount.lightvehicle += severityData[k].vehicledatum.lightvehicle_qty
        //             vehicleCount.heavyvehicle += severityData[k].vehicledatum.heavyvehicle_qty
        //             speedMean.motorcycle += severityData[k].vehicledatum.motorcycle_meanspeed
        //             speedMean.lightvehicle += severityData[k].vehicledatum.lightvehicle_meanspeed
        //             speedMean.heavyvehicle += severityData[k].vehicledatum.heavyvehicle_meanspeed
        //         }
        //     }
        // }

        const labelVehicle = ['Motorcycle', 'Light Vehicle', 'Heavy Vehicle']
        const labelStatus = ['A', 'B', 'C', 'D', 'E', 'F']

        const optPie = {
            maintainAspectRatio: false,
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                            return previousValue + currentValue;
                        });
                        var currentValue = dataset.data[tooltipItem.index];
                        var percentage = Math.floor(((currentValue / total) * 100) + 0.5);

                        return data.labels[tooltipItem.index] + ' ' + percentage + "%";
                    }
                }
            } 
        }

        const optBar = {
            maintainAspectRatio: false,
            tooltips: {
                callbacks: {
                    label: function (tooltipItem, data) {
                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var total = dataset.data.reduce(function (previousValue, currentValue, currentIndex, array) {
                            return previousValue + currentValue;
                        });
                        var currentValue = dataset.data[tooltipItem.index];
                        var percentage = Math.floor(((currentValue / total) * 100) + 0.5);

                        return currentValue.toFixed(2);
                    }
                }
            },
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }

        const dataQtyVeh = {
            labels: labelVehicle,
            datasets: [{
                label: 'Qty',
                data: Object.values(vehicleCount),
                backgroundColor: [
                    "#EC407A",
                    "#5C6BC0",
                    "#26C6DA"
                ],
                hoverBackgroundColor: [
                    "#E91E63",
                    "#3F51B5",
                    "#00BCD4"
                ]
            }]
        }

        const dataSpdVeh = {
            labels: labelVehicle,
            datasets: [{
                label: 'm/s',
                data: Object.values(speedMean),
                backgroundColor: [
                    "#EC407A",
                    "#5C6BC0",
                    "#26C6DA"
                ],
                hoverBackgroundColor: [
                    "#E91E63",
                    "#3F51B5",
                    "#00BCD4"
                ]
            }]
        }

        // const dataSevSta = {
        //     labels: labelStatus,
        //     datasets: [{
        //         label: 'Severity Level',
        //         data: Object.values(status),
        //         backgroundColor: [
        //             "#EC407A",
        //             "#5C6BC0",
        //             "#26C6DA",
        //             "#9CCC65",
        //             "#FFCA28",
        //             "#8D6E63"
        //         ],
        //         hoverBackgroundColor: [
        //             "#E91E63",
        //             "#3F51B5",
        //             "#00BCD4",
        //             "#8BC34A",
        //             "#FFC107",
        //             "#795548"
        //         ]
        //     }]
        // }

        return (
            <div className={classes.root}>
                <div className={classes.infoGraphTextGroup}>
                    <div className={classes.gridInfoItem}>
                        <Paper className={classes.infoGraphText} style={{ backgroundColor: '#FF7043' }} onClick={() => this.props.history.push('/A')}>
                            <div className={classes.infoText}>
                                <Typography variant="display3" style={{color: 'white'}}>
                                    {/* {status["A"]} */}
                                    <CountTo to={status["A"]} speed={1234} />
                                </Typography>
                                <Typography variant="body1" style={{ color: 'white' }}>Records</Typography>
                            </div>
                            <div className={classes.infoIcon}>
                                <Typography className={classes.iconInfo}>A</Typography>
                                {/* <FaceIcon className={classes.iconInfo} /> */}
                            </div>
                        </Paper>
                    </div>
                    <div className={classes.gridInfoItem}>
                        <Paper className={classes.infoGraphText} style={{ backgroundColor: '#FF5722' }} onClick={() => this.props.history.push('/B')}>
                            <div className={classes.infoText}>
                                <Typography variant="display3" style={{color: 'white'}}>
                                    {/* {nodeCount} */}
                                    <CountTo to={status["B"]} speed={1234} />
                                </Typography>
                                <Typography variant="body1" style={{ color: 'white' }}>Records</Typography>
                            </div>
                            <div className={classes.infoIcon}>
                                <Typography className={classes.iconInfo}>B</Typography>
                            </div>
                        </Paper>
                    </div>
                    <div className={classes.gridInfoItem}>
                        <Paper className={classes.infoGraphText} style={{ backgroundColor: '#F4511E' }} onClick={() => this.props.history.push('/C')}>
                            <div className={classes.infoText}>
                                <Typography variant="display3" style={{ color: 'white' }}>
                                    {/* {roadCount} */}
                                    <CountTo to={status["C"]} speed={1234} />
                                </Typography>
                                <Typography variant="body1" style={{ color: 'white' }}>Records</Typography>
                            </div>
                            <div className={classes.infoIcon}>
                                <Typography className={classes.iconInfo}>C</Typography>
                            </div>
                        </Paper>
                    </div>
                    <div className={classes.gridInfoItem}>
                        <Paper className={classes.infoGraphText} style={{ backgroundColor: '#E64A19' }} onClick={() => this.props.history.push('/D')}>
                            <div className={classes.infoText}>
                                <Typography variant="display3" style={{ color: 'white' }}>
                                    {/* {trafficCount} */}
                                    <CountTo to={status["D"]} speed={1234} />
                                </Typography>
                                <Typography variant="body1" style={{ color: 'white' }}>Records</Typography>
                            </div>
                            <div className={classes.infoIcon}>
                                <Typography className={classes.iconInfo}>D</Typography>
                            </div>
                        </Paper>
                    </div>
                    <div className={classes.gridInfoItem}>
                        <Paper className={classes.infoGraphText} style={{ backgroundColor: '#D84315' }} onClick={() => this.props.history.push('/E')}>
                            <div className={classes.infoText}>
                                <Typography variant="display3" style={{ color: 'white' }}>
                                    {/* {trafficCount} */}
                                    <CountTo to={status["E"]} speed={1234} />
                                </Typography>
                                <Typography variant="body1" style={{ color: 'white' }}>Records</Typography>
                            </div>
                            <div className={classes.infoIcon}>
                                <Typography className={classes.iconInfo}>E</Typography>
                            </div>
                        </Paper>
                    </div>
                    <div className={classes.gridInfoItem}>
                        <Paper className={classes.infoGraphText} style={{ backgroundColor: '#BF360C' }} onClick={() => this.props.history.push('/F')}>
                            <div className={classes.infoText}>
                                <Typography variant="display3" style={{ color: 'white' }}>
                                    {/* {trafficCount} */}
                                    <CountTo to={status["F"]} speed={1234} />
                                </Typography>
                                <Typography variant="body1" style={{ color: 'white' }}>Records</Typography>
                            </div>
                            <div className={classes.infoIcon}>
                                <Typography className={classes.iconInfo}>F</Typography>
                            </div>
                        </Paper>
                    </div>
                </div>
                
                {/* Chart or Severity Data */}
                <Switch>
                    {/* <div className={classes.gridChart}>
                        <div className={classes.gridChartItem}>
                            <Paper className={classes.chartPaperText} style={{ backgroundColor: '#F5F5F5' }}>
                                <div>
                                    <Typography variant="title" className={classes.chartTitle}>Vehicle Quantities</Typography>
                                </div>
                                <div style={{ height: 252 }} >
                                    <Pie
                                        height={252}
                                        data={dataQtyVeh}
                                        options={optPie}
                                    />
                                </div>
                            </Paper>
                        </div>
                        <div className={classes.gridChartItem}>
                            <Paper className={classes.chartPaperText} style={{ backgroundColor: '#F5F5F5' }}>
                                <div>
                                    <Typography variant="title" className={classes.chartTitle}>Vehicle Mean Speed</Typography>
                                </div>
                                <div style={{ height: 252 }} >
                                    <HorizontalBar
                                        height={252}
                                        data={dataSpdVeh}
                                        options={optBar}
                                    />
                                </div>
                            </Paper>
                        </div>
                    </div> */}
                    <Route
                        exact
                        path={'/'}
                        render={() => <GraphData qtyData={{...dataQtyVeh}} qtyOpt={{...optPie}} speedData={{...dataSpdVeh}} speedOpt={{...optBar}} />}
                    />

                    <Route
                        exact
                        path={'/:severityStat'}
                        component={SeverityData}
                    />
                </Switch>
            </div>
        )
    }
}

export default withStyles(styles)(HomePage)