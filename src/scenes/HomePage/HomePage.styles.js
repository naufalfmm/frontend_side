import grey from '@material-ui/core/colors/grey';

const styles = theme => ({
    root: {
        width: '100%',
        height: '100%',
        flexGrow: 1
    },
    //Infographics
    infoGraphTextGroup: {
        marginBottom: theme.spacing.unit,
        display: 'flex',
        flexWrap: 'wrap'
    },
    gridInfoItem: {
        flexGrow: 1,
        marginLeft: theme.spacing.unit,
        marginTop: theme.spacing.unit, 
        minWidth: 400
    },
    infoGraphText: {
        height: 150,
        '&:hover': {
            opacity: 0.8,
            cursor: 'pointer'
        }
    },
    infoText: {
        width: '50%',
        float: 'left',
        marginLeft: theme.spacing.unit * 2,
        top: '50%',
        transform: 'translateY(-50%)',
        position: 'relative'
    },
    infoIcon: {
        width: '30%',
        float: 'right',
        textAlign: 'right',
        marginRight: theme.spacing.unit * 2,
        top: '50%',
        transform: 'translateY(-50%)',
        position: 'relative',
    },
    iconInfo: {
        fontSize: ['Permanent Marker', 'Roboto'],
        fontSize: '4em',
        color: grey[400],
    },

    // Chart
    chartTextGroup: {
        marginBottom: theme.spacing.unit
    },
    gridChart: {
        marginTop: theme.spacing.unit * 2,
        display: 'flex',
        flexWrap: 'wrap'
    },
    gridChartItem: {
        flexGrow: 1, 
        flexBasis: '50%', 
        minWidth: 560,
        // margin: 8
    },
    chartPaperText: {
        height: 300,
        margin: theme.spacing.unit
    },
    chartTitle: {
        textAlign: 'center',
        padding: theme.spacing.unit
    },
})

export default styles