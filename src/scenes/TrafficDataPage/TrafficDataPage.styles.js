import lime from '@material-ui/core/colors/lime';

const styles = theme => ({
    tablePaper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    tablePaperHeader: {
        height: 80,
        '& h2': {
            position: 'relative',
            top: '50%',
            transform: 'translateY(-50%)',
            float: 'left'
        },
        '& button': {
            position: 'relative',
            marginLeft: theme.spacing.unit,
            marginRight: theme.spacing.unit,
            top: '50%',
            transform: 'translateY(-50%)',
            float: 'right',
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            '&:hover': {
                backgroundColor: lime[800]
            }
        }
    },
    expansionPanel: {
        marginBottom: theme.spacing.unit * 2
    },
    tableHeader: {
        '& th': {
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            textAlign: 'center',
            fontSize: '0.8rem'
        }
    },
    tablePaperContent: {
        marginTop: theme.spacing.unit * 2,
        boxShadow: 'none'
    },
    tableBody: {
        '& td': {
            textAlign: 'center',
            fontSize: '0.8rem'
        }
    },
    editDeleteIcon: {
        float: 'right',
    },
    circular: {
        left: '50%',
        position: 'relative',
        color: lime[900]
    },
    segmentDiv: {
        marginTop: theme.spacing.unit * 2
    }
})

export default styles