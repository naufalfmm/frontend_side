import lime from '@material-ui/core/colors/lime';

const styles = theme => ({
    properties: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: theme.spacing.unit
    },
    contentProperties: {
        flexBasis: '48%',
        flexGrow: 1,
        '& p': {
            fontSize: '0.9rem',
            marginBottom: theme.spacing.unit / 2
        },
        '&.right': {
            marginRight: theme.spacing.unit,
        },
        '&.left': {
            marginLeft: theme.spacing.unit,
        },
    },
    circular: {
        left: '35%',
        position: 'relative',
        color: lime[900]
    }
})

export default styles