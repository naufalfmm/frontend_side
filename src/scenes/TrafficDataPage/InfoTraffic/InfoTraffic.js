import React, { Component } from 'react';
import axios from '../../../axios-ins';
import { withStyles, CircularProgress, Typography, Dialog, DialogTitle, Divider, DialogContent } from '@material-ui/core';
import styles from './InfoTraffic.styles';

import Auxi from '../../../hoc/Auxi/Auxi';

class InfoTraffic extends Component {
    state = {
        loading: false,
        error: false,
        data: {}
    }

    componentDidMount() {
        console.log("InfoTraffic didMount")
        this.getDataHandler()
    }

    componentDidUpdate(prevProps) {
        // console.log("InfoRoad didUpdate openAdd", prevState.openAdd, this.state.openAdd)
        if (prevProps.match.params.trafficId !== this.props.match.params.trafficId) {
            console.log("InfoTraffic didUpdate", prevProps.match.params.trafficId, this.props.match.params.trafficId)
            this.getDataHandler()
        }
    }

    getDataHandler = () => {
        console.log("InfoTraffic getDataHandler", this.props.match.params)
        this.setState({ loading: true, error: false })
        axios.get('data/' + this.props.match.params.trafficId)
            .then(res => {
                let stateUpdate = {
                    loading: false,
                    error: false,
                    data: res.data.data
                }
                this.setState(stateUpdate)
            })
            .catch(() => {
                let stateUpdate = {
                    loading: false,
                    error: true,
                    data: {}
                }
                this.setState(stateUpdate)
            })
    }

    render() {
        const { classes } = this.props

        const data = {...this.state.data}

        console.log("InfoTraffic render", data)

        let form = (
            <Auxi>
                <CircularProgress className={classes.circular} size={60} />
            </Auxi>
        )

        if (!this.state.loading && !this.state.error) {
            console.log("In If", data)
            if (data.severitystatus_id !== undefined) {
                form = (
                    <Auxi>
                        <div className={classes.properties}>
                            <div className={[classes.contentProperties, "right"].join(' ')}>
                                <Typography style={{ fontWeight: 'bold', fontSize: '1.1rem', paddingBottom: 8 }}>Traffic Properties</Typography>
                                <Typography>{"Posting Time: " + data.received_time}</Typography>
                            </div>
                            <div className={[classes.contentProperties, "left"].join(' ')}>
                                <Typography style={{ fontWeight: 'bold', fontSize: '1.1rem', paddingBottom: 8 }}>Node Properties</Typography>
                                <Typography>{"Username: " + data.node.node_username}</Typography>
                                <Typography>{"Path/Lane: " + data.node.lane.path.path_th + "/" + data.node.lane.lane_th}</Typography>
                            </div>
                        </div>
                        <div className={classes.properties}>
                            <div className={[classes.contentProperties, "right"].join(' ')}>
                                <Typography style={{ fontWeight: 'bold', fontSize: '1.1rem', paddingBottom: 8 }}>Vehicles Quantity</Typography>
                                <Typography>{"Motorcycle: " + data.vehicledatum.motorcycle_qty}</Typography>
                                <Typography>{"Light Vehicle: " + data.vehicledatum.lightvehicle_qty}</Typography>
                                <Typography>{"Heavy Vehicle: " + data.vehicledatum.heavyvehicle_qty}</Typography>
                            </div>
                            <div className={[classes.contentProperties, "left"].join(' ')}>
                                <Typography style={{ fontWeight: 'bold', fontSize: '1.1rem', paddingBottom: 8 }}>Vehicles Speed Mean</Typography>
                                <Typography>{"Motorcycle: " + data.vehicledatum.motorcycle_meanspeed}</Typography>
                                <Typography>{"Light Vehicle: " + data.vehicledatum.lightvehicle_meanspeed}</Typography>
                                <Typography>{"Heavy Vehicle: " + data.vehicledatum.heavyvehicle_meanspeed}</Typography>
                            </div>
                        </div>
                    </Auxi>
                )
            }
        } else {
            form = null
        }

        return (
            <Dialog
                onClose={() => this.props.history.push('/traffic')}
                open
            >
                <DialogTitle>{"Info Traffic"}</DialogTitle>
                <Divider />
                <DialogContent className={classes.dialogContent}>
                    {form}
                </DialogContent>
            </Dialog>
        )
    }
}

export default withStyles(styles)(InfoTraffic)