import React, { Component } from 'react';
import axios from '../../axios-ins';
import moment from 'moment';
import { withStyles, Paper, Typography, Button, Divider,
        Table, TableHead, TableRow, TableCell, TableBody, TableData,
        ExpansionPanelSummary, ExpansionPanelDetails, ExpansionPanel,
        IconButton, 
        CircularProgress} from '@material-ui/core';
import { Route } from 'react-router-dom';

import styles from './TrafficDataPage.styles';

import RefreshIcon from '@material-ui/icons/Refresh';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import EditIcon from '@material-ui/icons/Edit';
import InfoIcon from '@material-ui/icons/Info';
import DeleteIcon from '@material-ui/icons/Delete';

import InfoTraffic from './InfoTraffic/InfoTraffic';
import Auxi from '../../hoc/Auxi/Auxi';

class TrafficData extends Component {
    state = {
        loading: false,
        dataResp: [],
        error: false
    }

    componentDidMount () {
        console.log("TrafficDataPage didMount", this.props)
        this.unlisten = this.props.history.listen((location, action) => {
            console.log("TrafficDataPage on route change");
            const pattern = /^(\/traffic)$/
            if (pattern.test(location.pathname)) this.getAllDataHandler()
        });
        this.getAllDataHandler()
    }

    componentWillUnmount() {
        console.log("TrafficDataPage WillUnmount")
        // this.unlisten()
    }

    getAllDataHandler = () => {
        console.log("TrafficDataPage start getAllDataHandler")
        this.setState({ loading: true, error: false })
        axios.get('data/all')
            .then(res => {
                this.setState({ loading: false, dataResp: res.data.data })
                console.log("TrafficDataPage finish getAllDataHandler")
            })
            .catch(e => {
                this.setState({ loading: false, error: false })
            })
    }

    render () {
        const { classes } = this.props

        let trafficData = this.state.dataResp.slice()

        let form = (
            <Auxi>
                <CircularProgress className={classes.circular} size={60} />
            </Auxi>
        )

        if (!this.state.loading) {
            let trafficShow = []
            if (trafficData.length > 0) {
                for (let i = 0; i < trafficData.length; i++) {
                    let segmentsShow = []
                    let segmentsData = trafficData[i].segments.slice()
                    for (let j = 0; j < segmentsData.length; j++) {
                        let severityShow = []
                        let pathsData = segmentsData[j].paths.slice()
                        for (let k = 0; k < pathsData.length; k++) {
                            let lanesData = pathsData[k].lanes.slice()
                            for (let l = 0; l < lanesData.length; l++) {
                                let severityData = lanesData[l].node.severitystatuses.slice()
                                for (let m = 0; m < severityData.length; m++) {
                                    severityShow.push(
                                        <TableRow key={m}>
                                            <TableCell>{moment(severityData[m].received_time).format("DD-MM-YYYY HH:mm:ss")}</TableCell>
                                            <TableCell>{lanesData[l].node.node_username}</TableCell>
                                            <TableCell>{pathsData[k].path_th + "/" + lanesData[l].lane_th}</TableCell>
                                            <TableCell>{severityData[m].status}</TableCell>
                                            <TableCell>
                                                <IconButton className={classes.editDeleteIcon} variant='fab' mini>
                                                    <InfoIcon onClick={() => this.props.history.push('/traffic/info/' + severityData[m].severitystatus_id)} />
                                                </IconButton>
                                            </TableCell>
                                        </TableRow>
                                    )
                                }
                            }
                        }
                        segmentsShow.push(
                            <Auxi key={j}>
                                <div className={classes.segmentDiv}>
                                    <Typography>{segmentsData[j].segment_start + " - " + segmentsData[j].segment_end}</Typography>
                                    <Table>
                                        <TableHead className={classes.tableHeader}>
                                            <TableRow>
                                                <TableCell>Posting Time</TableCell>
                                                <TableCell>Node</TableCell>
                                                <TableCell>Path/Lane</TableCell>
                                                <TableCell>Status</TableCell>
                                                <TableCell style={{ maxWidth: 25, margin: 'center' }} >Info</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody className={classes.tableBody}>
                                            {severityShow.slice()}
                                        </TableBody>
                                    </Table>
                                </div>
                            </Auxi>
                        )
                    }
                    trafficShow.push(
                        <ExpansionPanel key={i} className={classes.expansionPanel}>
                            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                <Typography>{"Jln. " + trafficData[i].road_name}</Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails style={{flexDirection: 'column'}}>
                                {segmentsShow.slice()}
                            </ExpansionPanelDetails>
                        </ExpansionPanel>
                    )
                }

                form = (
                    <Auxi>
                        {trafficShow.slice()}
                    </Auxi>
                )
            } else {
                form = null
            }
        }

        return (
            <div className={classes.root}>
                <Paper className={classes.tablePaper} elevation={1}>
                    <div className={classes.tablePaperHeader}>
                        <Typography variant="title">
                            Traffic Data Table
                        </Typography>`
                        <Button variant='fab' mini>
                            <RefreshIcon onClick={this.getAllDataHandler} />
                        </Button>
                    </div>
                    <Divider />
                    <Paper className={classes.tablePaperContent}>
                        {form}
                    </Paper>
                </Paper>

                <Route
                    path={this.props.match.path + '/info/:trafficId'}
                    component={InfoTraffic}
                />
            </div>
        )
    }
}

export default withStyles(styles)(TrafficData)