import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import axios from '../../axios-ins';
import { withStyles, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, 
    Paper, Typography, Button, Divider, Table, TableHead, TableBody, TableRow, 
    TableCell, IconButton, TextField, InputAdornment, FormLabel, FormControl, 
    RadioGroup, FormControlLabel, Radio, Select, MenuItem, 
    Dialog, DialogTitle, DialogContent, DialogActions, Menu, Fade, Grow, Popover } from '@material-ui/core';

import AddRoad from './AddRoad/AddRoad';
import InfoRoad from './InfoRoad/InfoRoad';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import AddIcon from '@material-ui/icons/Add';
import RefreshIcon from '@material-ui/icons/Refresh';
import FilterListIcon from '@material-ui/icons/FilterList';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import SearchIcon from '@material-ui/icons/Search';
import InfoIcon from '@material-ui/icons/Info';

import styles from './RoadPage.styles';
import Snackbar from '../../hoc/Snackbar/Snackbar';
import RoadExpansions from './RoadExpansions/RoadExpansions'
import Popper from 'popper.js';

class RoadPage extends Component {
    state = {
        filterState: false,
        filter: {
            roadName: '',
            population: '',
            segments: ''
        },
        editForm: {
            open: false,
            roadName: '',
            directions: '',
            lanes: '',
            population: '',
            density: ''
        },
        dataResp: [],
        dataFilter: [],
        error: false,
        errorResp: {},
        editData: {},
        formFilter: {
            lanes: [],
            population: [],
            density: []
        },
        anchorEl: null
    }

   defaultFilter = {
        roadName: '',
        population: '',
        segments: ''
    }

    componentDidMount () {
        console.log("RoadPage didMount")
        this.unlisten = this.props.history.listen((location, action) => {
            console.log("RoadPage on route change");
            const pattern = /^(\/road)/
            if (pattern.test(location.pathname)) this.getAllDataHandler()
        });
        this.getAllDataHandler()
        // this.props.history.replace('/road')
    }

    componentWillUnmount() {
        console.log("RoadPage WillUnmount")
        this.unlisten()
    }

    checkValidity(value, rules) {
        console.log(value, rules)
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.require) {
            isValid = value.toString().trim() !== '' && isValid;
        }

        return isValid;
    }

    infoRoadModalHandler = (roadIndex) => {
        this.props.history.replace('/road/info' + roadIndex.toString())
        // let editFormUpdate = { ...this.state.editForm }
        // editFormUpdate.open = !this.state.editForm.open
        // if (this.state.editForm.open) {
        //     editFormUpdate.roadName = ""
        //     editFormUpdate.directions = ""
        //     editFormUpdate.lanes = ""
        //     editFormUpdate.population = ""
        //     editFormUpdate.density = ""
        // } else {
        //     editFormUpdate.roadName = this.state.dataResp[roadIndex].road_name
        //     editFormUpdate.directions = this.state.dataResp[roadIndex].road_paths
        //     editFormUpdate.lanes = this.state.dataResp[roadIndex].road_lanes
        //     editFormUpdate.population = this.state.dataResp[roadIndex].road_population_range
        //     editFormUpdate.density = this.state.dataResp[roadIndex].road_density
        // }
        // this.setState({ editForm: editFormUpdate })
    }
    
    editRoadHandler = () => {
        this.setState({ error: false, errorResp: {} })
    }

    addRoadFormHandler = () => {
        // console.log("addRoadFormHandler", this.state.addForm.open)
        // let addFormUpdate = {...this.state.addForm}
        // addFormUpdate.open = !this.state.addForm.open
        // this.setState({addForm: addFormUpdate})
        this.props.history.replace('/road/add')
    }

    addRoadHandler = () => {
        // let addFormUpdate = {...this.state.addForm}
        // addFormUpdate.loading = true
        // addFormUpdate.error.stat = false
        // addFormUpdate.error.resp = {}
        // this.setState({ addForm: addFormUpdate })
        // var addData = {}
        // addData["name"] = this.state.addForm.roadName
        // addData["lanes"] = this.state.addForm.lanes
        // addData["paths"] = this.state.addForm.directions
        // addData["pop_range"] = this.state.addForm.population
        // addData["density"] = this.state.addForm.density

        // axios.post('/road/add/', addData)
        //     .then(() => {
        //         this.setState({
        //             addForm: {
        //                 loading: false,
        //                 focus: null,
        //                 error: {
        //                     stat: false,
        //                     open: false,
        //                     resp: {}
        //                 },
        //                 roadName: {
        //                     value: '',
        //                     valid: false,
        //                     touched: false,
        //                     validate: {
        //                         require: true
        //                     }
        //                 },
        //                 directions: {
        //                     value: '',
        //                     valid: false,
        //                     touched: false,
        //                     validate: {
        //                         require: true
        //                     }
        //                 },
        //                 lanes: {
        //                     value: '',
        //                     valid: false,
        //                     touched: false,
        //                     validate: {
        //                         require: true
        //                     }
        //                 },
        //                 population: {
        //                     value: '',
        //                     valid: false,
        //                     touched: false,
        //                     validate: {
        //                         require: true
        //                     }
        //                 },
        //                 density: {
        //                     value: '',
        //                     valid: false,
        //                     touched: false,
        //                     validate: {
        //                         require: true
        //                     }
        //                 }
        //             }
        //         })
        //         this.getAllDataHandler()
        //         this.props.history.replace('/road')
        //     })
        //     .catch(e => {
        //         console.log(e.response, e.request)
        //         if (e.response) {
        //             let addFormUpdate = { ...this.state.addForm }
        //             addFormUpdate.loading = false
        //             addFormUpdate.error.stat = true
        //             addFormUpdate.error.open = true
        //             addFormUpdate.error.resp = e.response.data
        //             this.setState({
        //                 addForm: addFormUpdate
        //             })
        //             // this.props.history.replace('/road')
        //         } else {
        //             let addFormUpdate = { ...this.state.addForm }
        //             addFormUpdate.loading = false
        //             addFormUpdate.error.stat = true
        //             addFormUpdate.error.open = true
        //             addFormUpdate.error.resp = e.request.data
        //             this.setState({
        //                 addForm: addFormUpdate
        //             })
        //             // this.props.history.replace('/road')
        //         }
        //     })
    }

    closeSnackbar = () => {
        let addFormUpdate = { ...this.state.addForm }
        addFormUpdate.error.open = false
        this.setState({
            addForm: addFormUpdate
        })
    }
    
    addRoadChangeHandler = (name, event) => {
        // event.preventDefault()
        // let addFormUpdate = {
        //     ...this.state.addForm,
        //     focus: name,
        //     [name]: {
        //         ...this.state.addForm[name],
        //         value: event.target.value,
        //         valid: this.checkValidity(event.target.value, this.state.addForm[name].validate),
        //         touched: true
        //     }
        // }
        // // addFormUpdate[name] = event.target.value
        // this.setState({ addForm: addFormUpdate })
    }

    selectFilterHandler = (data) => {
        // console.log("selectFilterHandler", data, data.length)
        let lanes = []
        let population = []
        let density = []

        for (let i=0; i<data.length; i++) {
            if (!(lanes.indexOf(data[i].road_lanes) > -1)) {
                lanes.push(data[i].road_lanes)
            }

            if (!(population.indexOf(data[i].road_population_range) > -1)) {
                population.push(data[i].road_population_range)
            }

            if (!(density.indexOf(data[i].road_density) > -1)) {
                density.push(data[i].road_density)
            }
        }

        const newFormFilter = {
            lanes: lanes,
            population: population,
            density: density
        }
        this.setState({formFilter: newFormFilter})
    }

    getAllDataHandler = () => {
        console.log("RoadPage start getAllDataHandler")
        this.setState({ error: false, errorResp: {} })
        axios.get('/road/all')
            .then(res => {
                this.setState({ dataResp: res.data.road_full })
                // this.selectFilterHandler(res.data.road_data)
                console.log("RoadPage finish getAllDataHandler")
            })
            .catch(e => {
                if (e.response) {
                    this.setState({
                        error: true,
                        errorResp: e.response
                    })
                }
            })
    }

    deleteDataHandler = (road_id) => {
        this.setState({ error: false, errorResp: {} })
        axios.delete('/road/delete/'+road_id)
            .then(() => {
                this.getAllDataHandler()
            })
            .catch(e => {
                if (e.response) {
                    this.setState({
                        error: true,
                        errorResp: e.response
                    })
                }
            })
    }

    filterHandler = () => {
        this.setState({filterState: true})

        const data = this.state.dataResp.slice()
        const dataShow = []
        
        for (let i=0; i<data.length; i++) {
            if (this.state.filter.roadName !== '') {
                let re = new RegExp('^('+this.state.filter.roadName.toLowerCase()+')')
                if ((!re.test(data[i].road_name.toLowerCase()))) continue
            }

            if (this.state.filter.population !== '') {
                if (data[i].road_population_range !== parseInt(this.state.filter.population)) continue
            }

            if (this.state.filter.segments !== '') {
                if (data[i].segments.length !== parseInt(this.state.filter.segments)) continue
            }
            // console.log(data[i].road_paths, this.state.filter.directions, data[i].road_paths !== this.state.filter.directions)
            // if (this.state.filter.directions !== '') {
            //     if (data[i].road_paths !== parseInt(this.state.filter.directions,10)) continue
            // }

            // if (this.state.filter.lanes !== '') {
            //     if (data[i].road_lanes !== this.state.filter.lanes) continue
            // }

            // if (this.state.filter.density !== '') {
            //     if (data[i].road_density !== this.state.filter.density) continue
            // }

            dataShow.push(data[i])
        }

        this.setState({dataFilter: dataShow})
        this.filterMenuHandler()
        // this.selectFilterHandler(dataShow)
    }

    resetFilterHandler = () => {
        console.log('resetFilter')
        this.setState({
            filterState: false,
            filter: {...this.defaultFilter},
            dataFilter: []
        })
        // console.log(this.state.filter)
        // this.selectFilterHandler(this.state.dataResp)
        this.filterMenuHandler()
    }

    changeHandler = name => event => {
        let filterUpdate = {...this.state.filter}
        filterUpdate[name] = event.target.value
        this.setState({filter: filterUpdate})
    }

    filterMenuHandler = (event) => {
        if (this.state.anchorEl === null)
            this.setState({ anchorEl: event.currentTarget })
        else
            this.setState({ anchorEl: null })
    }

    render () {
        const { classes } = this.props
        const populationRng = ["< 0.1", "0.1 - 0.5", "0.5 - 1.0", "1.0 - 3.0", "> 3.0"]
        // const rdDensity = ["Very Low", "Low", "Moderate", "High", "Very High"]

        let dataShow = this.state.dataResp.slice()

        if (this.state.filterState) {
            dataShow = this.state.dataFilter.slice()
        }

        let tableData = []

        if (dataShow.length > 0) {
            for (let i=0; i<dataShow.length; i++) {
                tableData.push(
                    <TableRow key={dataShow[i].road_id}>
                        <TableCell style={{ textAlign: 'left' }} >Jln. {dataShow[i].road_name}</TableCell>
                        <TableCell>{dataShow[i].segments.length}</TableCell>
                        <TableCell>{populationRng[dataShow[i].road_population_range]}</TableCell>
                        <TableCell>
                            {
                                this.props.adminStatus !== 'admin' ?
                                    (
                                        <IconButton className={classes.editDeleteIcon} variant='fab' mini>
                                            <DeleteIcon onClick={() => this.deleteDataHandler(dataShow[i].road_id)} />
                                        </IconButton>
                                    )
                                : null
                            }
                            <IconButton className={classes.editDeleteIcon} variant='fab' mini>
                                <InfoIcon onClick={() => this.props.history.push('/road/info/' + dataShow[i].road_id.toString())} 
                                />
                            </IconButton>
                        </TableCell>
                    </TableRow>
                )
            }
        }

        // let itemLanes = []
        // const lanes = this.state.formFilter.lanes.slice().sort()

        // for (let i=0; i<lanes.length; i++) {
        //     itemLanes.push(
        //         <MenuItem key={i} value={lanes[i]}>{lanes[i]}</MenuItem>
        //     )
        // }

        // let itemPopulation = []
        // const population = this.state.formFilter.population.slice().sort()

        // // console.log(populationRng, population)

        // for (let i = 0; i < population.length; i++) {
        //     itemPopulation.push(
        //         <MenuItem key={i} value={population[i]}>{populationRng[population[i]]}</MenuItem>
        //     )
        // }

        // let itemDensity = []
        // const density = this.state.formFilter.density.slice().sort()

        // // console.log(rdDensity, density)

        // for (let i = 0; i < density.length; i++) {
        //     itemDensity.push(
        //         <MenuItem key={i} value={density[i]}>{rdDensity[density[i]]}</MenuItem>
        //     )
        // }
        console.log(this.state.dataResp)
        return (
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={1}>
                    <div className={classes.paperHeader}>
                        <Typography variant="title">
                            Road Data Table
                        </Typography>
                        <Button variant='fab' mini>
                            <RefreshIcon onClick={this.getAllDataHandler} />
                        </Button>
                        {
                            this.props.adminStatus !== 'admin' ?
                                <Button variant='fab' mini>
                                    <AddIcon onClick={this.addRoadFormHandler} />
                                </Button>
                            : null
                        }
                        <Button
                            aria-owns={this.state.anchorEl ? 'filter-form' : null}
                            aria-haspopup="true"
                            variant='fab' 
                            mini
                            onClick={this.filterMenuHandler}
                        >
                            <FilterListIcon  />
                        </Button>
                        <Popover
                            id="filter-form"
                            open={Boolean(this.state.anchorEl)}
                            anchorEl={this.state.anchorEl}
                            onClose={this.filterMenuHandler}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'center',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'center',
                            }}
                        >
                            <Paper className={classes.filterForm}>
                                <div className={classes.filterHeader}>
                                    <Typography variant="title">Road Filter</Typography>
                                    <Divider />
                                </div>
                                <div className={classes.filterBody}>
                                    <FormControl className={classes.filterText}>
                                        <FormLabel component="legend">Name</FormLabel>
                                        <TextField
                                            id="roadName"
                                            name="roadName" 
                                            value={this.state.filter.roadName}
                                            onChange={this.changeHandler('roadName')}
                                            InputProps={{
                                                startAdornment: <InputAdornment position="start">Jln</InputAdornment>
                                            }}
                                        />
                                    </FormControl>
                                    <FormControl className={classes.filterText}>
                                        <FormLabel component="legend">Number of Segments</FormLabel>
                                        <TextField
                                            id="segmentCount"
                                            name="segmentCount"
                                            type="number"
                                            value={this.state.filter.segments}
                                            onChange={this.changeHandler('segments')}
                                        />
                                    </FormControl>
                                    <FormControl className={classes.filterText}>
                                        <FormLabel component="legend">Population Range</FormLabel>
                                        <TextField
                                            select
                                            name="population"
                                            value={this.state.filter.population}
                                            onChange={this.changeHandler('population')}
                                            InputProps={{
                                                endAdornment: <InputAdornment position="start">Billion</InputAdornment>,
                                            }}
                                        >
                                            <MenuItem value='' />
                                            <MenuItem value={0}>&lt; 0.1</MenuItem>
                                            <MenuItem value={1}>0.1 - 0.5</MenuItem>
                                            <MenuItem value={2}>0.5 - 1.0</MenuItem>
                                            <MenuItem value={3}>1.0 - 3.0</MenuItem>
                                            <MenuItem value={4}>&gt; 3.0</MenuItem>
                                            </TextField>
                                    </FormControl>
                                    <Divider />
                                </div>
                                <div className={classes.filterActions}>
                                    <Button variant='contained' onClick={this.filterHandler}>
                                        Search
                                    </Button>
                                    <Button variant='contained' onClick={this.resetFilterHandler}>
                                        Reset
                                    </Button>
                                </div>
                            </Paper>
                        </Popover>
                    </div>
                    <Divider />
                    <Paper className={classes.paperContent}>
                        {/* <RoadExpansions roads={this.state.dataResp.slice()} /> */}
                        <Table>
                            <TableHead className={classes.tableHeader}>
                                <TableRow>
                                    <TableCell>Road Name</TableCell>
                                    <TableCell numeric>Number of Segments</TableCell>
                                    <TableCell>Population Range<br />(billion)</TableCell>
                                    <TableCell style={{ width: 96 }} >Actions</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody className={classes.tableBody}>
                                {tableData}
                            </TableBody>
                        </Table>
                    </Paper>

                </Paper>
                {/* <ExpansionPanel
                    className={classes.expansionPanel}
                >
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Typography variant="title">
                            Filter Road Table
                    </Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <div className={classes.filterTextGroup}>
                            <FormControl className={classes.textFilter}>
                                <FormLabel component="legend">Road Name</FormLabel>
                                <TextField
                                    id="roadName"
                                    name="roadName"
                                    value={this.state.filter.roadName}
                                    onChange={this.changeHandler('roadName')}
                                    InputProps={{
                                        startAdornment: <InputAdornment position="start">Jln</InputAdornment>
                                    }}
                                />
                            </FormControl>
                            <FormControl component="fieldset" className={classes.textFilter}>
                                <FormLabel component="legend">Number of Directions</FormLabel>
                                <RadioGroup
                                    aria-label="Directions"
                                    name="directions"
                                    value={this.state.filter.directions}
                                    className={classes.radioButtonGroup}
                                    onChange={this.changeHandler('directions')}
                                >
                                    <FormControlLabel value="1" control={<Radio />} label="One-Way" />
                                    <FormControlLabel value="2" control={<Radio />} label="Two-Way" />
                                </RadioGroup>
                            </FormControl>
                            <FormControl className={classes.textFilter}>
                                <FormLabel component="legend">Number of Lanes</FormLabel>
                                <Select 
                                    name="lanes"
                                    value={this.state.filter.lanes}
                                    onChange={this.changeHandler('lanes')}    
                                >
                                    <MenuItem value='' />
                                    {itemLanes}
                                    {/* <MenuItem value={2}>2</MenuItem>
                                    <MenuItem value={4}>4</MenuItem>
                                    <MenuItem value={6}>6</MenuItem> */}
                                {/* </Select>
                            </FormControl>
                            <FormControl className={classes.textFilter}>
                                <FormLabel component="legend">Population Range</FormLabel>
                                <TextField
                                    select
                                    name="population"
                                    value={this.state.filter.population}
                                    onChange={this.changeHandler('population')}
                                    InputProps={{
                                        endAdornment: <InputAdornment position="start">Billion</InputAdornment>,
                                    }}
                                >
                                    <MenuItem value='' />
                                    {itemPopulation}
                                    {/* <MenuItem value="< 0.1">&lt; 0.1</MenuItem>
                                    <MenuItem value="0.1 - 0.5">0.1 - 0.5</MenuItem>
                                    <MenuItem value="0.5 - 1.0">0.5 - 1.0</MenuItem>
                                    <MenuItem value="1.0 - 3.0">1.0 - 3.0</MenuItem>
                                    <MenuItem value="> 3.0">&gt; 3.0</MenuItem> */}
                                {/* </TextField>
                            </FormControl>
                            <FormControl className={classes.textFilter}>
                                <FormLabel component="legend">Roadside Density</FormLabel>
                                <TextField
                                    select
                                    name="density"
                                    value={this.state.filter.density}
                                    onChange={this.changeHandler('density')}
                                >
                                    <MenuItem value='' />
                                    {itemDensity}
                                    {/* <MenuItem value="vl">Very Low</MenuItem>
                                    <MenuItem value="l">Low</MenuItem>
                                    <MenuItem value="m">Moderate</MenuItem>
                                    <MenuItem value="h">High</MenuItem>
                                    <MenuItem value="vh">Very High</MenuItem> */}
                                {/* </TextField>
                            </FormControl>
                        </div>
                        <div className={classes.filterTextButton}>
                            <Button variant='fab' mini>
                                <SearchIcon onClick={this.filterHandler} />
                            </Button>
                            <Button variant='fab' mini>
                                <RemoveIcon onClick={this.resetFilterHandler}/>
                            </Button>
                        </div>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <Paper className={classes.tablePaper} elevation={1}>
                    <div className={classes.tablePaperHeader}>
                        <Typography variant="title">
                            Road Data Table
                        </Typography>
                        <Button variant='fab' mini>
                            <RefreshIcon onClick={this.getAllDataHandler} />
                        </Button>
                        <Button variant='fab' mini>
                            <AddIcon onClick={() => this.addRoadFormHandler(true)} />
                        </Button>
                    </div>
                    <Divider />
                    <Paper className={classes.tablePaperContent}>
                        <Table>
                            <TableHead className={classes.tableHeader}>
                                <TableRow>
                                    <TableCell>Road Name</TableCell>
                                    <TableCell numeric>Paths</TableCell>
                                    <TableCell numeric>Lanes</TableCell>
                                    <TableCell>Population Range<br />(billion)</TableCell>
                                    <TableCell>Roadside Density</TableCell>
                                    <TableCell style={{ width: 96 }} >Actions</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody className={classes.tableBody}>
                                {tableData}
                            </TableBody>
                        </Table>
                    </Paper>
                </Paper> */}

                {/* Info Road */}
                <Switch>
                    <Route
                        path={this.props.match.path + '/info/:roadId'}
                        component={InfoRoad}
                    />
                    
                    { /* Add Road */
                        this.props.adminStatus !== 'admin' ?
                            <Route
                                path={this.props.match.path + '/add'}
                                component={AddRoad}
                            />
                        : null 
                    }
                </Switch>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        adminStatus: state.auth.status
    }
}

export default connect(mapStateToProps, null)(withStyles(styles)(RoadPage))