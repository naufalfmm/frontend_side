import React from 'react';

import RoadExpansion from './RoadExpansion/RoadExpansion';

const roadExpansions = (props) => {
    console.log(props)
    let roads = []
    for (let i=0; i<props.roads.length; i++) {
        console.log(i, props.roads[i])
        roads.push(
            <RoadExpansion road={{...props.roads[i]}} />
        )
    }
    return roads
}

export default roadExpansions