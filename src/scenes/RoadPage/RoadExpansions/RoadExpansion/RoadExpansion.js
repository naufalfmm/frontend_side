import React from 'react';
import { withStyles, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Typography, Table, TableHead, TableRow, TableCell, IconButton, Divider, TableBody, Button } from '@material-ui/core';
import styles from './RoadExpansion.styles';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';

const roadExpansion = (props) => {
    const { classes } = props
    console.log("roadExpansion", props)
    const populationRng = ["< 0.1", "0.1 - 0.5", "0.5 - 1.0", "1.0 - 3.0", "> 3.0"]
    const sgDensity = ["Very Low", "Low", "Moderate", "High", "Very High"]
    const segmentData = props.road.segments.slice()
    let segmentShow = []

    for (let i=0; i<segmentData.length; i++) {
        let lanes = ""
        if (segmentData[i].segment_paths === 2) {
            lanes = segmentData[i].paths[0].path_th === 0 ? segmentData[i].paths[0].path_lanes.toString() + "/" + segmentData[i].paths[1].path_lanes.toString() : segmentData[i].paths[1].path_lanes.toString() + "/" + segmentData[i].paths[0].path_lanes.toString()
        } else {
            lanes = segmentData[i].paths[0].path_lanes.toString() + "/-"
        }
        
        segmentShow.push(
            <TableRow key={segmentData[i].segment_id}>
                <TableCell>{segmentData[i].segment_start}</TableCell>
                <TableCell>{segmentData[i].segment_end}</TableCell>
                <TableCell>{segmentData[i].segment_paths}</TableCell>
                <TableCell>{lanes}</TableCell>
                <TableCell>{sgDensity[segmentData[i].segment_density]}</TableCell>
                <TableCell>
                    <IconButton className={classes.buttonFloat} variant="fab" mini>
                        <DeleteIcon />
                    </IconButton>
                    <IconButton className={classes.buttonFloat} variant="fab" mini>
                        <EditIcon />
                    </IconButton>
                </TableCell>
            </TableRow>
        )
    }
    return (
        <ExpansionPanel key={props.road.road_id} className={classes.expansionPanel}>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <div className={classes.column}>
                    <Typography className={classes.titleExpansion}>{"Jln. " + props.road.road_name}</Typography>
                </div>
                <div className={classes.column} />
                <div className={classes.column}>
                    <IconButton style={{float: "right"}} variant="fab" mini>
                        <DeleteIcon />
                    </IconButton>
                </div>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails style={{flexDirection: 'column'}}>
                <div className={classes.roadProp}>
                    <Typography>{"Population Range: " + populationRng[props.road.road_population_range] + " billion"}</Typography>
                    <div className={classes.buttonsProp}>
                        <Button variant="fab" mini>
                            <AddIcon />
                        </Button>
                    </div>
                </div>
                <Table>
                    <TableHead className={classes.tableHeader}>
                        <TableRow>
                            <TableCell>Segment Start</TableCell>
                            <TableCell>Segment End</TableCell>
                            <TableCell>Segment Paths</TableCell>
                            <TableCell>Segment Lanes</TableCell>
                            <TableCell>Segment Density</TableCell>
                            <TableCell style={{ maxWidth: 96, margin: 'center' }} >Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody className={classes.tableBody}>
                        {segmentShow.slice()}
                    </TableBody>
                </Table>
            </ExpansionPanelDetails>
        </ExpansionPanel>
    )
}

export default withStyles(styles)(roadExpansion)