import lime from '@material-ui/core/colors/lime';

const styles = theme => ({
    column: {
        flexBasis: '33.33%'
    },
    expansionPanel: {
        marginBottom: theme.spacing.unit * 2
    },
    titleExpansion: {
        top: '50%',
        transform: 'translateY(-50%)',
        position: 'relative'
    },
    tableHeader: {
        '& th': {
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            textAlign: 'center',
            fontSize: '0.8rem'
        }
    },
    buttonFloat: {
        float: 'right'
    },
    tableHeader: {
        '& th': {
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            textAlign: 'center',
            fontSize: '0.8rem'
        }
    },
    tableBody: {
        '& td': {
            textAlign: 'center',
            fontSize: '0.8rem'
        }
    },
    roadProp: {
        display: 'flex',
        marginBottom: theme.spacing.unit * 2,
        '& p': {
            width: '60%',
            transform: 'translateY(30%)'
        }
    },
    buttonsProp: {
        width: '40%',
        '& button': {
            float: 'right',
            marginLeft: theme.spacing.unit,
            marginRight: theme.spacing.unit,
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            '&:hover': {
                backgroundColor: lime[800]
            }
        }
    }
})

export default styles