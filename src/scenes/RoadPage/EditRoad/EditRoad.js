import React, { PureComponent } from 'react';
import axios from '../../../axios-ins';
import {
    withStyles, FormControl, FormLabel, TextField,
    InputAdornment, MenuItem, Divider, Button,
    RadioGroup, FormControlLabel, Radio, Paper,
    Typography, CircularProgress, Dialog, DialogTitle,
    DialogContent, DialogActions
} from '@material-ui/core';

import styles from './EditRoad.styles';
import RoadSim from '../../../containers/SegmentSimulation/SegmentSimulation';
import Auxi from '../../../hoc/Auxi/Auxi';

class EditRoad extends PureComponent {
    state = {
        loading: false,
        error: false,
        form: {
            roadName: {
                value: '',
                valid: false,
                touched: false,
                validate: {
                    require: true
                }
            },
            population: {
                value: '',
                valid: false,
                touched: false,
                validate: {
                    require: true
                }
            },
        }
    }

    componentDidMount() {
        console.log("EditRoad didMount", this.props.data)
        this.setDataHandler()
    }

    setDataHandler = () => {
        let formUpdate = {
            roadName: {
                ...this.state.form.roadName,
                value: this.props.roadData.road_name
            },
            population: {
                ...this.state.form.population,
                value: this.props.roadData.road_population_range
            }
        }

        this.setState({ form: formUpdate })
    }

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.require) {
            isValid = value.toString().trim() !== '' && isValid;
        }

        if (rules.min) {
            isValid = value >= rules.min && isValid
        }

        if (rules.max) {
            isValid = value <= rules.max && isValid
        }

        return isValid;
    }

    submitHandler = () => {
        this.setState({ loading: true })
        let dataReq = {
            'name': this.state.form.roadName.value,
            'pop_range': this.state.form.population.value
        }

        axios.post('/road/edit/' + this.props.roadData.road_id, dataReq)
            .then(res => {
                let formUpdate = {
                    roadName: {
                        ...this.state.form.roadName,
                        value: this.props.roadData.road_name,
                        valid: false,
                        touched: false
                    },
                    population: {
                        ...this.state.form.population,
                        value: this.props.roadData.road_population_range,
                        valid: false,
                        touched: false
                    }
                }

                this.setState({ form: formUpdate, loading: false })
                this.props.reloadData()
                this.props.close()
            })
            .catch(e => {
                this.setState({ loading: false, error: true })
            })
    }

    changeHandler = (name, event) => {
        event.preventDefault()
        let formUpdate = {
            ...this.state.form,
            [name]: {
                ...this.state.form[name],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.form[name].validate),
                touched: true
            }
        }  
        this.setState({ form: formUpdate })
    }

    formHandler = () => {
        this.props.history.push('/road')
    }

    render() {
        const { classes } = this.props

        let form = (
            <Auxi>
                <CircularProgress className={classes.circular} size={60} />
            </Auxi>
        )

        if (!this.state.loading) {
            const disableButton = !(
                (this.state.form.roadName.valid || !this.state.form.roadName.touched) &&
                (this.state.form.population.valid || !this.state.form.population.touched)
            )
            form = (
                <Auxi>
                    <div className={classes.paperHeader} >
                        <Typography variant="title">Edit Road</Typography>
                        <Divider />
                    </div>
                    <div className={classes.paperContent}>
                        <FormControl required className={classes.content}>
                            <FormLabel component="legend">Road Name</FormLabel>
                            <TextField
                                required
                                error={!this.state.form.roadName.valid && this.state.form.roadName.touched}
                                id="road-name"
                                name="road-name"
                                value={this.state.form.roadName.value}
                                onChange={(event) => this.changeHandler('roadName', event)}
                                InputProps={{
                                    startAdornment: <InputAdornment position="start">Jln</InputAdornment>
                                }}
                            />
                        </FormControl>
                        <FormControl required className={classes.content}>
                            <FormLabel component="legend">Population Range</FormLabel>
                            <TextField
                                select
                                required
                                error={!this.state.form.population.valid && this.state.form.population.touched}
                                name="population"
                                value={this.state.form.population.value}
                                onChange={(event) => this.changeHandler('population', event)}
                                InputProps={{
                                    endAdornment: <InputAdornment position="start">Billion</InputAdornment>,
                                }}
                            >
                                <MenuItem value='' />
                                <MenuItem value={0}>&lt; 0.1</MenuItem>
                                <MenuItem value={1}>0.1 - 0.5</MenuItem>
                                <MenuItem value={2}>0.5 - 1.0</MenuItem>
                                <MenuItem value={3}>1.0 - 3.0</MenuItem>
                                <MenuItem value={4}>&gt; 3.0</MenuItem>
                            </TextField>
                        </FormControl>
                    </div>
                    <div className={classes.paperButton} >
                        <Button
                            color="primary"
                            disabled={disableButton}
                            onClick={this.submitHandler}>Edit</Button>
                        <Button color="primary" onClick={this.props.close}>Cancel</Button>
                    </div>
                </Auxi>
            )
        }

        return (
            <Paper className={classes.paper}>
                {/* {snackbar} */}
                {form}
                {/* {this.state.redirect ? <Redirect to="/road" /> : null} */}
            </Paper>
        )
    }
}

export default withStyles(styles)(EditRoad)