import grey from '@material-ui/core/colors/grey';
import lime from '@material-ui/core/colors/lime';

const styles = theme => ({
    paper: {
        paddingLeft: theme.spacing.unit * 2,
        paddingRight: theme.spacing.unit * 2,
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    paperHeader: {
        marginBottom: theme.spacing.unit * 2,
        '& h2': {
            fontSize: '1.1rem',
            marginBottom: theme.spacing.unit * 2
        }
    },
    paperContent: {
        marginBottom: theme.spacing.unit * 2,
        display: 'flex',
        flexDirection: 'column'
    },
    radioButtonGroup: {
        display: 'flex',
        flexDirection: 'row'
    },
    content: {
        marginBottom: theme.spacing.unit * 2
    },
    paperButton: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        '& button': {
            marginTop: theme.spacing.unit,
            marginBottom: theme.spacing.unit,
            marginLeft: theme.spacing.unit,
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            '&:hover': {
                backgroundColor: lime[800]
            },
            '&[disabled]': {
                backgroundColor: grey[600]
            }
        }
    },
    circular: {
        color: lime[900]
    }
})

export default styles