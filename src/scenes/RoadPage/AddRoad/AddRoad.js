import React, { Component } from 'react';
import axios from '../../../axios-ins';
import { withStyles, FormControl, FormLabel, TextField, 
        InputAdornment, MenuItem, Divider, Button, 
        RadioGroup, FormControlLabel, Radio, Paper, 
        Typography, CircularProgress, Snackbar, Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';

import styles from './AddRoad.styles';
import RoadSim from '../../../containers/SegmentSimulation/SegmentSimulation';
import Auxi from '../../../hoc/Auxi/Auxi';

class AddRoad extends Component {
    state = {
        loading: false,
        error: false,
        // ret: null,
        // error: {
        //     stat: false,
        //     open: false,
        //     resp: {}
        // },
        // success: {
        //     stat: false,
        //     open: false,
        //     resp: {}
        // },
        form: {
            roadName: {
                value: '',
                valid: false,
                touched: false,
                validate: {
                    require: true
                }
            },
            population: {
                value: '',
                valid: false,
                touched: false,
                validate: {
                    require: true
                }
            }
        }
    }

    componentDidMount() {
        console.log("AddRoad didMount")
    }

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.require) {
            isValid = value.toString().trim() !== '' && isValid;
        }

        return isValid;
    }

    formHandler = () => {
        // this.props.history.push('/road')
        // return (<Redirect to="/road" />)
        // this.setState({redirect: true})
        this.props.history.push('/road')
    }

    changeHandler = (name, event) => {
        event.preventDefault()
        let formUpdate = {
            ...this.state.form,
            [name]: {
                ...this.state.form[name],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.form[name].validate),
                touched: true
            }
        }
        // addFormUpdate[name] = event.target.value
        this.setState({ form: formUpdate })
    }

    submitHandler = () => {
        console.log("submitHandler")
        this.setState({ loading: true, error: false })

        var addData = {}
        addData["name"] = this.state.form.roadName.value
        addData["pop_range"] = this.state.form.population.value

        axios.post('/road/add/', addData)
            .then(() => {
                this.setState({ loading: false, error: false })
                this.props.history.push('/road')
                // return(<Redirect to="/road" />)
            })
            .catch(() => {
                this.setState({ loading: false, error: true })
            })
    }

    render () {
        const { classes } = this.props

        // let snackbar = null
        // if (this.state.ret !== null) {
        //     const rets = this.state.ret
        //     console.log(this.state[rets].resp)
        //     snackbar = (
        //         <Snackbar
        //             anchorOrigin={{
        //                 vertical: 'top',
        //                 horizontal: 'center',
        //             }}
        //             variant={rets}
        //             open={this.state.ret !== null}
        //             autoHideDuration={6000}
        //             onClose={this.snackbarHandler}
        //             ContentProps={{
        //                 'aria-describedby': 'message-id',
        //             }}
        //             message={<span id="message-id">{this.state[rets].resp.message}</span>}
        //         />
        //     )
        // }

        let form = (
                <Auxi>
                    <CircularProgress className={classes.circular} size={60} />
                </Auxi>
            )

        let actions = null

        let disableButton = (
            !(this.state.form.roadName.valid && this.state.form.roadName.touched) ||
            !(this.state.form.population.valid && this.state.form.population.touched)
        )

        if (!this.state.loading) {
            form = (
                <Auxi>
                    <div className={classes.contentForm}>
                        <FormControl required className={[classes.content, "right"].join(' ')}>
                            <FormLabel component="legend">Road Name</FormLabel>
                            <TextField
                                required
                                error={(!this.state.form.roadName.valid && this.state.form.roadName.touched)}
                                id="roadName"
                                name="roadName"
                                value={this.state.form.roadName.value}
                                onChange={(event) => this.changeHandler('roadName', event)}
                                InputProps={{
                                    startAdornment: <InputAdornment position="start">Jln.</InputAdornment>,
                                }}
                            />
                        </FormControl>
                        <FormControl required className={classes.content}>
                            <FormLabel component="legend">Population Range</FormLabel>
                            <TextField
                                select
                                required
                                error={!this.state.form.population.valid && this.state.form.population.touched}
                                name="population"
                                value={this.state.form.population.value}
                                onChange={(event) => this.changeHandler('population', event)}
                                InputProps={{
                                    endAdornment: <InputAdornment position="start">Billion</InputAdornment>,
                                }}
                            >
                                <MenuItem value='' />
                                <MenuItem value={0}>&lt; 0.1</MenuItem>
                                <MenuItem value={1}>0.1 - 0.5</MenuItem>
                                <MenuItem value={2}>0.5 - 1.0</MenuItem>
                                <MenuItem value={3}>1.0 - 3.0</MenuItem>
                                <MenuItem value={4}>&gt; 3.0</MenuItem>
                            </TextField>
                        </FormControl>
                    </div>
                </Auxi>
            )

            actions = (
                <Auxi>
                    <Divider />
                    <DialogActions className={classes.dialogActions}>
                        <Button
                            color="primary"
                            disabled={disableButton}
                            onClick={this.submitHandler}
                        >
                            Add
                            </Button>
                        <Button color="primary" onClick={() => this.props.history.push('/road')}>Cancel</Button>
                    </DialogActions>
                </Auxi>
            )
        }

        // console.log("redirect", this.state.redirect)

        return (
            <Dialog
                onClose={() => this.props.history.push('/road')}
                open
            >
                <DialogTitle>{"Add Road"}</DialogTitle>
                <Divider />
                <DialogContent className={classes.dialogContent}>
                    {form}
                </DialogContent>
                {actions}
            </Dialog>
        )
    }
}

export default withStyles(styles)(AddRoad)