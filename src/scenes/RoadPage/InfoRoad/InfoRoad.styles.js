import lime from '@material-ui/core/colors/lime';

const styles = theme => ({
    paper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    paperHeader: {
        minHeight: 40,
        display: 'flex',
        paddingBottom: theme.spacing.unit,
    },
    paperHeaderText: {
        fontSize: '1.1rem',
        flexBasis: '66.66%',
        transform: 'translateY(20%)'
    },
    paperHeaderActions: {
        flexBasis: '33.33%',
        '& button': {
            width: 40,
            height: 40,
            boxShadow: '0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 6px 10px 0px rgba(0, 0, 0, 0.14), 0px 1px 18px 0px rgba(0, 0, 0, 0.12)',
            marginLeft: theme.spacing.unit,
            marginRight: theme.spacing.unit,
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            float: 'right',
            '&:hover': {
                backgroundColor: lime[800]
            }
        }
    },
    paperContent: {
        marginTop: theme.spacing.unit * 2
    },
    tableHeader: {
        '& th': {
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            textAlign: 'center',
            fontSize: '0.8rem'
        }
    },
    tableBody: {
        '& td': {
            textAlign: 'center',
            fontSize: '0.8rem'
        }
    },
    roadProperties: {
        marginBottom: theme.spacing.unit * 2,
        display: 'flex'
    },
    roadPropertiesData: {
        flexBasis: '66.66%',
        '& p': {
            fontSize: '0.9rem',
        }
    },
    roadPropertiesActions: {
        flexBasis: '33.33%',
        '& button': {
            transform: 'translateY(70%)',
            width: 40,
            height: 40,
            boxShadow: '0px 3px 5px -1px rgba(0, 0, 0, 0.2), 0px 6px 10px 0px rgba(0, 0, 0, 0.14), 0px 1px 18px 0px rgba(0, 0, 0, 0.12)',
            marginLeft: theme.spacing.unit,
            marginRight: theme.spacing.unit,
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            float: 'right',
            '&:hover': {
                backgroundColor: lime[800]
            }
        }
    },
    segmentData: {
        marginTop: theme.spacing.unit * 2,
        overflow: 'auto'
    },
    circular: {
        left: '45%',
        position: 'relative',
        color: lime[900]
    }
})

export default styles