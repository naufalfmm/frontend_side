import React, { Component } from 'react';
import axios from '../../../axios-ins';
import { withStyles, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, Typography, Table, TableHead, TableRow, TableCell, IconButton, Divider, TableBody, Button, Paper, CircularProgress, Popover } from '@material-ui/core';
import { Redirect } from 'react-router-dom';

import styles from './InfoRoad.styles';

import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import RefreshIcon from '@material-ui/icons/Refresh';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';

import EditRoad from '../EditRoad/EditRoad';
import AddSegment from '../AddSegment/AddSegment';
import EditSegment from '../EditSegment/EditSegment';
import Auxi from '../../../hoc/Auxi/Auxi';

class InfoRoad extends Component {
    state = {
        loading: false,
        error: false,
        roadData: {},
        anchorEl: null,
        openAdd: false,
        openEdit: false,
        editIdx: 0
    }

    componentDidMount() {
        console.log("InfoRoad didMount")
        this.getDataHandler()
    }

    componentDidUpdate(prevProps, prevState) {
        // console.log("InfoRoad didUpdate openAdd", prevState.openAdd, this.state.openAdd)
        if (prevProps.match.params.roadId !== this.props.match.params.roadId || 
            ((prevState.openAdd !== this.state.openAdd) && !this.state.openAdd) ||
            ((prevState.openEdit !== this.state.openEdit) && !this.state.openEdit)) {
            console.log("InfoRoad didUpdate", prevProps.match.params.roadId, this.props.match.params.roadId)
            this.getDataHandler()
        }
    }

    getDataHandler = () => {
        console.log("InfoRoad getDataHandler", this.state)
        this.setState({ loading: true })
        axios.get('/road/' + this.props.match.params.roadId)
            .then(res => {
                let stateUpdate = {...this.state}
                stateUpdate.loading = false
                stateUpdate.roadData = res.data.data
                stateUpdate.error = false
                console.log("InfoRoad getDataHandler then", res.data.data)
                this.setState(stateUpdate)
            })
            .catch(err => {
                let stateUpdate = { ...this.state }
                stateUpdate.loading = false
                stateUpdate.error = true
                this.setState(stateUpdate)
            })
    }

    editMenuHandler = (event) => {
        if (this.state.anchorEl === null)
            this.setState({ anchorEl: event.currentTarget })
        else
            this.setState({ anchorEl: null })
    }
    
    closeEditMenu = () => {
        this.setState({ anchorEl: null })
    }

    dialogAddSegmentHandler = () => {
        let stateUpdate = {...this.state}
        stateUpdate.openAdd = !stateUpdate.openAdd
        this.setState(stateUpdate)
    }

    dialogEditSegmentHandler = (idx) => {
        let stateUpdate = { ...this.state }
        stateUpdate.openEdit = !stateUpdate.openEdit
        stateUpdate.editIdx = idx
        console.log(idx, this.state.roadData)
        this.setState(stateUpdate)
    }

    deleteSegment = (segmentId) => {
        axios.delete('segment/delete/' + segmentId)
            .then(() => {
                this.getDataHandler()
            })
    }
 
    render() {
        const { classes } = this.props

        const populationRng = ["< 0.1", "0.1 - 0.5", "0.5 - 1.0", "1.0 - 3.0", "> 3.0"]
        const sgDensity = ["Very Low", "Low", "Moderate", "High", "Very High"]

        let roadData = {}
        if (this.state.roadData !== {}) {
            roadData = {...this.state.roadData}
        }
        console.log("InfoRoad render", roadData, this.state)

        let form = (
            <Auxi>
                <CircularProgress className={classes.circular} size={60} />
            </Auxi>
        )

        // console.log(roadData !== {}, roadData)
        if (!this.state.loading && !this.state.error && roadData.segments !== undefined) {
            let table = null
            console.log(roadData)
            if (roadData.segments.length > 0) {
                let tableData = []
                const segmentData = roadData.segments.slice()
                console.log("segmentData", segmentData)

                for (let i = 0; i < segmentData.length; i++) {
                    let lanes = ""
                    if (segmentData[i].segment_paths === 2) {
                        lanes = segmentData[i].paths[0].path_th === 0 ? segmentData[i].paths[0].path_lanes.toString() + "/" + segmentData[i].paths[1].path_lanes.toString() : segmentData[i].paths[1].path_lanes.toString() + "/" + segmentData[i].paths[0].path_lanes.toString()
                    } else {
                        lanes = segmentData[i].paths[0].path_lanes.toString() + "/-"
                    }

                    tableData.push(
                        <TableRow key={segmentData[i].segment_id}>
                            <TableCell>{segmentData[i].segment_start}</TableCell>
                            <TableCell>{segmentData[i].segment_end}</TableCell>
                            <TableCell>{segmentData[i].segment_paths}</TableCell>
                            <TableCell>{lanes}</TableCell>
                            <TableCell>{sgDensity[segmentData[i].segment_density]}</TableCell>
                            <TableCell>
                                <IconButton className={classes.buttonFloat} variant="fab" mini>
                                    <EditIcon onClick={() => this.dialogEditSegmentHandler(i)} />
                                </IconButton>
                                <IconButton className={classes.buttonFloat} variant="fab" mini>
                                    <DeleteIcon onClick={() => this.deleteSegment(segmentData[i].segment_id)} />
                                </IconButton>
                            </TableCell>
                        </TableRow>
                    )
                }

                table = (
                    <Table>
                        <TableHead className={classes.tableHeader}>
                            <TableRow>
                                <TableCell>Segment Start</TableCell>
                                <TableCell>Segment End</TableCell>
                                <TableCell>Segment Paths</TableCell>
                                <TableCell>Segment Lanes</TableCell>
                                <TableCell>Segment Density</TableCell>
                                <TableCell style={{ maxWidth: 96, margin: 'center' }} >Actions</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody className={classes.tableBody}>
                            {tableData.slice()}
                        </TableBody>
                    </Table>
                )
            }
            
            form = (
                <Auxi>
                    <div className={classes.paperHeader}>
                        <Typography className={classes.paperHeaderText} variant="subheading">Segment Data</Typography>
                        <div className={classes.paperHeaderActions}>
                            <IconButton mini>
                                <CloseIcon onClick={() => this.props.history.push('/road')} />
                            </IconButton>
                            <IconButton mini>
                                <RefreshIcon onClick={this.getDataHandler} />
                            </IconButton>
                            <IconButton mini>
                                <AddIcon onClick={this.dialogAddSegmentHandler} />
                            </IconButton>
                        </div>
                    </div>
                    <Divider />
                    <div className={classes.paperContent}>
                        <div className={classes.roadProperties}>
                            <div className={classes.roadPropertiesData}>
                                <Typography style={{ fontWeight: 'bold', fontSize: '1.1rem', paddingBottom: 8 }}>Road Properties</Typography>
                                <Typography>{"Name: Jln. " + roadData.road_name}</Typography>
                                <Typography>{"Population Range: " + populationRng[roadData.road_population_range] + " billion"}</Typography>
                            </div>
                            <div className={classes.roadPropertiesActions}>
                                <IconButton mini>
                                    <EditIcon
                                        aria-owns={this.state.anchorEl ? 'edit-road' : null}
                                        aria-haspopup="true"
                                        onClick={this.editMenuHandler}
                                     />
                                </IconButton>
                            </div>
                            <Popover
                                id="filter-form"
                                open={Boolean(this.state.anchorEl)}
                                anchorEl={this.state.anchorEl}
                                onClose={this.editMenuHandler}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'right',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                            >
                                <EditRoad 
                                    roadData={
                                        {
                                            road_id: roadData.road_id, 
                                            road_name: roadData.road_name, 
                                            road_population_range: roadData.road_population_range
                                        }
                                    }
                                    reloadData={this.getDataHandler}
                                    close={this.closeEditMenu}
                                />
                            </Popover>
                        </div>
                        <Divider />
                        <div className={classes.segmentData}>
                            {table}
                        </div>
                    </div>
                </Auxi>
            )
        }

        if (this.state.error) {
            form = (
                <Redirect to="/road" />
            )
        }

        console.log("InfoRoad render openEdit", this.state.openEdit, this.state.editData)

        let editModal = null
        if (this.state.openEdit) {
            let editData = {}
            let segmentsData = {...roadData.segments[this.state.editIdx]}

            const segmentId = segmentsData.segment_id
            editData.start = segmentsData.segment_start
            editData.end = segmentsData.segment_end
            editData.paths = segmentsData.segment_paths
            editData.density = segmentsData.segment_density

            let firstIdx = 0
            let pathsData = segmentsData.paths.slice()
            if (pathsData[firstIdx].path_th === 1) firstIdx = 1
            

            editData.firstId = pathsData[firstIdx].path_id
            editData.firstLane = pathsData[firstIdx].path_lanes
            

            if (segmentsData.paths.length === 2) {
                let secondIdx = 1
                if (firstIdx === 1) secondIdx = 0
                editData.secondId = pathsData[secondIdx].path_id
                editData.secondLane = pathsData[secondIdx].path_lanes
            }

            console.log("openEdit", segmentsData, editData)

            editModal = (
                <EditSegment
                    open={this.state.openEdit}
                    segmentId={segmentId}
                    data={{...editData}}
                    closeHandler={this.dialogEditSegmentHandler}
                    online={true}
                />
            )
        }

        return (
            <Paper className={classes.paper}>
                {form}
                <AddSegment 
                    open={this.state.openAdd} 
                    roadId={this.props.match.params.roadId} 
                    closeHandler={this.dialogAddSegmentHandler}
                    online={true} 
                />
                {editModal}
            </Paper>
        )
    }
}

// const infoRoad = (props) => {
//     const { classes } = props
//     console.log("infoRoad", props)
//     const populationRng = ["< 0.1", "0.1 - 0.5", "0.5 - 1.0", "1.0 - 3.0", "> 3.0"]
//     const sgDensity = ["Very Low", "Low", "Moderate", "High", "Very High"]
//     const segmentData = props.road.segments.slice()
//     let segmentShow = []

//     for (let i = 0; i < segmentData.length; i++) {
//         let lanes = ""
//         if (segmentData[i].segment_paths === 2) {
//             lanes = segmentData[i].paths[0].path_th === 0 ? segmentData[i].paths[0].path_lanes.toString() + "/" + segmentData[i].paths[1].path_lanes.toString() : segmentData[i].paths[1].path_lanes.toString() + "/" + segmentData[i].paths[0].path_lanes.toString()
//         } else {
//             lanes = segmentData[i].paths[0].path_lanes.toString() + "/-"
//         }

//         segmentShow.push(
//             <TableRow key={segmentData[i].segment_id}>
//                 <TableCell>{segmentData[i].segment_start}</TableCell>
//                 <TableCell>{segmentData[i].segment_end}</TableCell>
//                 <TableCell>{segmentData[i].segment_paths}</TableCell>
//                 <TableCell>{lanes}</TableCell>
//                 <TableCell>{sgDensity[segmentData[i].segment_density]}</TableCell>
//                 <TableCell>
//                     <IconButton className={classes.buttonFloat} variant="fab" mini>
//                         <DeleteIcon />
//                     </IconButton>
//                     <IconButton className={classes.buttonFloat} variant="fab" mini>
//                         <EditIcon />
//                     </IconButton>
//                 </TableCell>
//             </TableRow>
//         )
//     }
//     return (
//         <Paper className={classes.paper}>
//             <div className={classes.paperHeader}>
//                 <Typography variant="subheading">Segment Data</Typography>
//             </div>
//         </Paper>
//         <ExpansionPanel key={props.road.road_id} className={classes.expansionPanel}>
//             <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
//                 <div className={classes.column}>
//                     <Typography className={classes.titleExpansion}>{"Jln. " + props.road.road_name}</Typography>
//                 </div>
//                 <div className={classes.column} />
//                 <div className={classes.column}>
//                     <IconButton style={{ float: "right" }} variant="fab" mini>
//                         <DeleteIcon />
//                     </IconButton>
//                 </div>
//             </ExpansionPanelSummary>
//             <ExpansionPanelDetails style={{ flexDirection: 'column' }}>
//                 <div className={classes.roadProp}>
//                     <Typography>{"Population Range: " + populationRng[props.road.road_population_range] + " billion"}</Typography>
//                     <div className={classes.buttonsProp}>
//                         <Button variant="fab" mini>
//                             <AddIcon />
//                         </Button>
//                     </div>
//                 </div>
//                 <Table>
//                     <TableHead className={classes.tableHeader}>
//                         <TableRow>
//                             <TableCell>Segment Start</TableCell>
//                             <TableCell>Segment End</TableCell>
//                             <TableCell>Segment Paths</TableCell>
//                             <TableCell>Segment Lanes</TableCell>
//                             <TableCell>Segment Density</TableCell>
//                             <TableCell style={{ maxWidth: 96, margin: 'center' }} >Actions</TableCell>
//                         </TableRow>
//                     </TableHead>
//                     <TableBody className={classes.tableBody}>
//                         {segmentShow.slice()}
//                     </TableBody>
//                 </Table>
//             </ExpansionPanelDetails>
//         </ExpansionPanel>
//     )
// }

export default withStyles(styles)(InfoRoad)