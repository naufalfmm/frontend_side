import React, { Component } from 'react';
import axios from '../../../axios-ins';
import { withStyles, CircularProgress, FormControl, FormLabel, TextField, MenuItem, RadioGroup, FormControlLabel, Divider, Radio, DialogActions, Button, Dialog, DialogTitle, DialogContent } from '@material-ui/core';
import styles from './EditSegment.styles';

import SegmentSimulation from '../../../containers/SegmentSimulation/SegmentSimulation';
import Auxi from '../../../hoc/Auxi/Auxi';

class EditSegment extends Component {
    state = {
        loading: false,
        error: false,
        form: {
            start: {
                value: '',
                valid: true,
                touched: true,
                validate: {
                    required: true
                }
            },
            end: {
                value: '',
                valid: true,
                touched: true,
                validate: {
                    required: true
                }
            },
            paths: {
                value: '',
                valid: true,
                touched: true,
                validate: {
                    required: true
                }
            },
            density: {
                value: '',
                valid: true,
                touched: true,
                validate: {
                    required: true
                }
            },
            firstLane: {
                value: '',
                valid: true,
                touched: true,
                validate: {
                    required: true,
                    isNumber: true,
                    min: 1
                }
            },
            secondLane: {
                value: '',
                valid: true,
                touched: true,
                validate: {
                    required: true,
                    isNumber: true,
                    min: 1
                }
            }
        }
    }

    defaultForm = {
        start: {
            value: '',
            valid: true,
            touched: true,
            validate: {
                required: true
            }
        },
        end: {
            value: '',
            valid: true,
            touched: true,
            validate: {
                required: true
            }
        },
        paths: {
            value: '',
            valid: true,
            touched: true,
            validate: {
                required: true
            }
        },
        density: {
            value: '',
            valid: true,
            touched: true,
            validate: {
                required: true
            }
        },
        firstLane: {
            value: '',
            valid: true,
            touched: true,
            validate: {
                required: true,
                isNumber: true,
                min: 1
            }
        },
        secondLane: {
            value: '',
            valid: true,
            touched: true,
            validate: {
                required: true,
                isNumber: true,
                min: 1
            }
        }
    }

    componentDidMount() {
        console.log("EditSegment didMount", this.state)
        this.setFirstData(this.props.data)
    }

    componentDidUpdate(prevProps) {
        if (prevProps.open !== this.props.open) {
            console.log("EditSegment didUpdate", prevProps.open, this.props.open)
            if(!this.props.open) this.resetForm()
            else this.setFirstData(this.props)
        }
    }

    setFirstData = (data) => {
        let formUpdate = {...this.state.form}

        formUpdate.start.value = data.start
        formUpdate.end.value = data.end
        formUpdate.paths.value = data.paths.toString()
        formUpdate.density.value = data.density
        formUpdate.firstLane.value = data.firstLane.toString()
        if (data.secondLane !== undefined) formUpdate.secondLane.value = data.secondLane.toString()

        this.setState({ form: formUpdate })
    }

    resetForm = () => {
        this.setState({ form: this.defaultForm })
    }

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.toString().trim() !== '' && isValid;
        }

        if (rules.isNumber) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.min) {
            isValid = value >= rules.min && isValid
        }

        console.log(value.toString().trim() !== '', isValid)

        return isValid;
    }

    changeHandler = (name, event) => {
        event.preventDefault()
        console.log(name, event.target.value)
        let formUpdate = {
            ...this.state.form,
            [name]: {
                ...this.state.form[name],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.form[name].validate),
                touched: true
            }
        }
        this.setState({ form: formUpdate })
    }

    submitHandler = () => {
        this.setState({ loading: true })
        const data = { ...this.state.form }
        let postData = {
            start: data.start.value,
            end: data.end.value,
            density: data.density.value,
            paths: parseInt(data.paths.value),
            firstPath: {
                id: this.props.data.firstId,
                th: 0,
                lanes: parseInt(data.firstLane.value)
            }
        }
        if (parseInt(data.paths.value) > 1) {
            postData.secondPath = {
                id: this.props.data.secondId,
                th: 1,
                lanes: parseInt(data.secondLane.value)
            }
        }

        console.log("EditSegment submitHandler", postData)

        if (this.props.online) {
            axios.post('segment/edit/' + this.props.segmentId, postData)
                .then(() => {
                    this.setState({ loading: false, error: false })
                    this.props.closeHandler()
                })
                .catch(() => {
                    this.setState({ loading: false, error: true })
                })
        } else {
            this.props.submitHandler(postData)
            this.setState({ loading: false })
        }
    }

    render() {
        const { classes } = this.props

        let form = (
            <Auxi>
                <CircularProgress className={classes.circular} size={60} />
            </Auxi>
        )

        let actions = null

        let disableButton = (
            !(this.state.form.start.valid && this.state.form.start.touched) ||
            !(this.state.form.end.valid && this.state.form.end.touched) ||
            !(this.state.form.paths.valid && this.state.form.paths.touched) ||
            !(this.state.form.density.valid && this.state.form.density.touched)
        )

        if (!this.state.loading) {
            let segmentData = [0, 0]
            if (this.state.form.firstLane.value !== "" && this.state.form.firstLane.valid) segmentData[0] = parseInt(this.state.form.firstLane.value)
            if (this.state.form.secondLane.value !== "" && this.state.form.secondLane.valid && parseInt(this.state.form.paths.value) === 2) segmentData[1] = parseInt(this.state.form.secondLane.value)

            // console.log((
            //     (!this.state.form.start.valid || !this.state.form.start.touched) &&
            //     (!this.state.form.end.valid || !this.state.form.end.touched) &&
            //     (!this.state.form.paths.valid || !this.state.form.paths.touched) &&
            //     (!this.state.form.density.valid || !this.state.form.density.touched)
            // ),
            //     this.state.form.start.valid, this.state.form.start.touched,
            //     this.state.form.end.valid, this.state.form.end.touched,
            //     this.state.form.paths.valid, this.state.form.paths.touched,
            //     this.state.form.density.valid, this.state.form.density.touched,
            //     this.state.form.firstLane.valid, this.state.form.firstLane.touched,
            //     this.state.form.secondLane.valid, this.state.form.secondLane.touched, false && false && false && false
            // )

            console.log("EditSegment",
                this.state.form.start.valid, this.state.form.start.touched,
                this.state.form.end.valid, this.state.form.end.touched,
                this.state.form.paths.valid, this.state.form.paths.touched,
                this.state.form.density.valid, this.state.form.density.touched,
                this.state.form.firstLane.valid, this.state.form.firstLane.touched,
                this.state.form.secondLane.valid, this.state.form.secondLane.touched
            )

            let lanesForm = null
            if (parseInt(this.state.form.paths.value) === 1) {
                disableButton = (
                    !(this.state.form.start.valid && this.state.form.start.touched) ||
                    !(this.state.form.end.valid && this.state.form.end.touched) ||
                    !(this.state.form.paths.valid && this.state.form.paths.touched) ||
                    !(this.state.form.density.valid && this.state.form.density.touched) ||
                    !(this.state.form.firstLane.valid && this.state.form.firstLane.touched)
                )
                console.log(1, disableButton)
                lanesForm = (
                    <FormControl required className={[classes.content, "right", "lane"].join(' ')}>
                        <FormLabel component="legend">First Path (Number of Lanes)</FormLabel>
                        <TextField
                            required
                            type="number"
                            error={!(this.state.form.firstLane.valid && this.state.form.firstLane.touched)}
                            id="firstLane"
                            name="firstLane"
                            value={this.state.form.firstLane.value}
                            onChange={(event) => this.changeHandler('firstLane', event)}
                        />
                    </FormControl>
                )
            } else if (parseInt(this.state.form.paths.value) === 2) {
                console.log(2)
                disableButton = (
                    !(this.state.form.start.valid && this.state.form.start.touched) ||
                    !(this.state.form.end.valid && this.state.form.end.touched) ||
                    !(this.state.form.paths.valid && this.state.form.paths.touched) ||
                    !(this.state.form.density.valid && this.state.form.density.touched) ||
                    !(this.state.form.firstLane.valid && this.state.form.firstLane.touched) ||
                    !(this.state.form.secondLane.valid && this.state.form.secondLane.touched)
                )
                console.log(2, disableButton)
                lanesForm = (
                    <Auxi>
                        <FormControl id="firstLane" required className={[classes.content, "right", "lane"].join(' ')}>
                            <FormLabel component="legend">First Path (Number of Lanes)</FormLabel>
                            <TextField
                                required
                                type="number"
                                error={!(this.state.form.firstLane.valid && this.state.form.firstLane.touched)}
                                id="firstLane"
                                name="firstLane"
                                value={this.state.form.firstLane.value}
                                onChange={(event) => this.changeHandler('firstLane', event)}
                            />
                        </FormControl>
                        <FormControl id="secondLane" required className={[classes.content, "left", "lane"].join(' ')}>
                            <FormLabel component="legend">Second Path (Number of Lanes)</FormLabel>
                            <TextField
                                required
                                type="number"
                                error={!(this.state.form.secondLane.valid && this.state.form.secondLane.touched)}
                                id="secondLane"
                                name="secondLane"
                                value={this.state.form.secondLane.value}
                                onChange={(event) => this.changeHandler('secondLane', event)}
                            />
                        </FormControl>
                    </Auxi>
                )
            }

            form = (
                <Auxi>
                    <div className={classes.contentSimulation}>
                        <SegmentSimulation lane={segmentData.slice()} />
                    </div>
                    <div className={classes.contentForm}>
                        <FormControl required className={[classes.content, "right"].join(' ')}>
                            <FormLabel component="legend">Segment Start</FormLabel>
                            <TextField
                                required
                                error={!(this.state.form.start.valid && this.state.form.start.touched)}
                                id="start"
                                name="start"
                                value={this.state.form.start.value}
                                onChange={(event) => this.changeHandler('start', event)}
                            />
                        </FormControl>
                        <FormControl required className={[classes.content, "left"].join(' ')}>
                            <FormLabel component="legend">Segment End</FormLabel>
                            <TextField
                                required
                                error={!(this.state.form.end.valid && this.state.form.end.touched)}
                                id="end"
                                name="end"
                                value={this.state.form.end.value}
                                onChange={(event) => this.changeHandler('end', event)}
                            />
                        </FormControl>
                        <FormControl required className={[classes.content, "right"].join(' ')}>
                            <FormLabel component="legend">Roadside Density</FormLabel>
                            <TextField
                                select
                                required
                                error={!(this.state.form.density.valid && this.state.form.density.touched)}
                                name="density"
                                value={this.state.form.density.value}
                                onChange={(event) => this.changeHandler('density', event)}
                            >
                                <MenuItem value="" />
                                <MenuItem value={0}>Very Low</MenuItem>
                                <MenuItem value={1}>Low</MenuItem>
                                <MenuItem value={2}>Moderate</MenuItem>
                                <MenuItem value={3}>High</MenuItem>
                                <MenuItem value={4}>Very High</MenuItem>
                            </TextField>
                        </FormControl>
                        <FormControl
                            component="fieldset"
                            error={!(this.state.form.paths.valid && this.state.form.paths.touched)}
                            required
                            className={[classes.content, "left"].join(' ')}
                        >
                            <FormLabel component="legend">Number of Paths</FormLabel>
                            <RadioGroup
                                aria-label="paths"
                                name="paths"
                                value={this.state.form.paths.value}
                                className={classes.radioButtonGroup}
                                onChange={(event) => this.changeHandler('paths', event)}
                            >
                                <FormControlLabel value="1" control={<Radio />} label="One-Way" />
                                <FormControlLabel value="2" control={<Radio />} label="Two-Way" />
                            </RadioGroup>
                        </FormControl>
                        {lanesForm}
                    </div>
                </Auxi>
            )

            actions = (
                <Auxi>
                    <Divider />
                    <DialogActions className={classes.dialogActions}>
                        <Button
                            color="primary"
                            disabled={disableButton}
                            onClick={this.submitHandler}
                        >
                            Edit
                            </Button>
                        <Button color="primary" onClick={this.props.closeHandler}>Cancel</Button>
                    </DialogActions>
                </Auxi>
            )
        }

        return (
            <Dialog
                open={this.props.open}
                onClose={this.props.closeHandler}
            >
                <DialogTitle>{"Edit Segment"}</DialogTitle>
                <Divider />
                <DialogContent className={classes.dialogContent}>
                    {form}
                </DialogContent>
                {actions}
            </Dialog>
        )
    }
}

export default withStyles(styles)(EditSegment)