import lime from '@material-ui/core/colors/lime';

import 'typeface-roboto';

const styles = theme => ({
    root: {
        width: '100%',
        height: '100%',
    },
    filterForm: {
        paddingLeft: theme.spacing.unit * 2,
        paddingRight: theme.spacing.unit * 2,
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2
    },
    filterHeader: {
        marginBottom: theme.spacing.unit * 2,
        '& h2': {
            fontSize: '1.1rem',
            marginBottom: theme.spacing.unit * 2
        }
    },
    filterBody: {
        marginBottom: theme.spacing.unit * 2,
        display: 'flex',
        flexDirection: 'column'
    },
    filterText: {
        marginBottom: theme.spacing.unit * 2
    },
    filterActions: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        '& button': {
            marginTop: theme.spacing.unit,
            marginBottom: theme.spacing.unit,
            marginLeft: theme.spacing.unit,
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            '&:hover': {
                backgroundColor: lime[800]
            }
        }
    },
    expansionTable: {
        cursor: 'not-allowed'
    },
    expansionPanel: {
        width: '100%'
    },
    paper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    paperHeader: {
        height: 80,
        '& h2': {
            position: 'relative',
            top: '50%',
            transform: 'translateY(-50%)',
            float: 'left'
        },
        '& button': {
            position: 'relative',
            marginLeft: theme.spacing.unit,
            marginRight: theme.spacing.unit,
            top: '50%',
            transform: 'translateY(-50%)',
            float: 'right',
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            '&:hover': {
                backgroundColor: lime[800]
            }
        }
    },
    paperContent: {
        marginTop: theme.spacing.unit * 2,
        overflow: 'auto'
    },
    tableHeader: {
        '& th': {
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            textAlign: 'center',
            fontSize: '0.8rem'
        }
    },
    tableBody: {
        '& td': {
            textAlign: 'center',
            fontSize: '0.8rem'
        }
    },
    editDeleteIcon: {
        float: 'right',
    },
    filterTextGroup: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        justifyContent: 'space-between',
        width: '95%',
        alignItems: 'center'
        // margin: theme.spacing.unit
    },
    dialog: {
        height: 300
    },
    dialogContent: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        justifyContent: 'space-between',
        width: '95%',
        alignItems: 'center',
        alignContent: 'space-between',
        height: 'auto',
        marginBottom: theme.spacing.unit * 4
    },
    contentOfDialogContent: {
        flex: '0 48%',
        marginTop: theme.spacing.unit,
        width: '40%'
        // paddingRight: theme.spacing.unit
    },
    textField: {
        // flexBasis: 200,
    },
    radioButtonGroup: {
        display: 'flex',
        flexDirection: 'row'
    },
    laneData: {
        marginTop: theme.spacing.unit * 4,
        marginBottom: theme.spacing.unit * 4
    },
    contentOfLaneData: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        justifyContent: 'space-between',
        width: '95%',
        alignItems: 'center',
        alignContent: 'space-between',
        height: 'auto',
    }
    // addButton: {
    //     position: 'relative',
    //     float: 'right'
    // }
    // fabButton: {
    //     position: 'fixed',
    //     backgroundColor: lime[900],
    //     color: lime[500],
    //     bottom: theme.spacing.unit * 2,
    //     right: theme.spacing.unit * 2,
    //     '&:hover': {
    //         backgroundColor: lime[800]
    //     }
    // }
})

export default styles