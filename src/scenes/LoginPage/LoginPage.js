import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Paper, Typography, withStyles, TextField,
        FormControl, FormLabel, Button, CircularProgress } from '@material-ui/core';

import styles from './LoginPage.styles';
import Snackbar from '../../hoc/Snackbar/Snackbar';
import Auxi from '../../hoc/Auxi/Auxi';
import * as actions from '../../store/actions/auth';

class LoginPage extends Component {
    state = {
        username: '',
        password: '',
        error: false,
        loading: false
    }

    loginAdminHandler = () => {
        this.props.onAuth(this.state.username, this.state.password)
    }

    loginChangeHandler = (name, event) => {
        this.setState({ [name]: event.target.value })
    }

    render () {
        const { classes } = this.props

        let form = (
            <Auxi>
                <CircularProgress className={classes.circular} size={60} />
            </Auxi>
        )

        console.log("LoginPage render", this.props.loading, this.props.isAuthenticated, this.props.token)

        if (!this.props.loading) {
            form = (
                <div className={classes.loginInput}>
                    <FormControl className={classes.loginText}>
                        <TextField
                            id="username"
                            name="username"
                            label="Username"
                            value={this.state.username}
                            onChange={(event) => this.loginChangeHandler('username', event)}
                        />
                    </FormControl>
                    <FormControl className={classes.loginText}>
                        <TextField
                            id="password"
                            name="password"
                            label="Password"
                            type="password"
                            value={this.state.password}
                            onChange={(event) => this.loginChangeHandler('password', event)}
                        />
                    </FormControl>
                    <Button className={classes.loginButton} onClick={this.loginAdminHandler}>
                        Login
                        </Button>
                </div>
            )
        }

        if (this.props.isAuthenticated) {
            console.log("LoginPage", this.props.path)
            if (this.props.path === '/login') {
                form = (
                    <Redirect to='/' />
                )
            } else if (this.props.path === '/admin' && this.props.adminStatus !== 'superadmin') {
                form = (
                    <Redirect to='/' />
                )
            } else {
                form = (
                    <Redirect to={this.props.path} />
                )
            }
        }

        return (
            <div className={classes.root}>
                <Paper className={classes.paper} elevation={1}>
                    <Typography variant="headline" component="h3">
                        Congestion Detection System
                    </Typography>
                    {form}
                </Paper>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        isAuthenticated: state.auth.token !== null,
        path: state.auth.authRedirectPath,
        adminStatus: state.auth.status
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (email, password) => dispatch(actions.auth(email, password))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(LoginPage))