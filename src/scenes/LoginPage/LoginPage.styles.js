import lime from '@material-ui/core/colors/lime';

const styles = theme => ({
    root: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        minHeight: '100vh',
    },
    paper: {
        width: 350,
        height: 'auto',
        padding: theme.spacing.unit * 2
    },
    loginInput: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    loginText: {
        width: '100%',
        marginBottom: theme.spacing.unit * 2
    },
    loginButton: {
        width: '10%',
        backgroundColor: lime[900],
        color: theme.palette.common.white,
        '&:hover': {
            backgroundColor: lime[800]
        }
    },
    circular: {
        left: '0%',
        position: 'relative',
        color: lime[900]
    }
})

export default styles