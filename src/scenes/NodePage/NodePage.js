import React, { Component } from 'react';
import { connect } from 'react-redux'
import axios from '../../axios-ins';
import { saveAs } from 'file-saver/FileSaver';
import { Route, Switch } from 'react-router-dom';

import styles from './NodePage.styles';
import { withStyles, Paper, Typography, Button, Divider,
        Table, TableRow, TableCell, TableHead, TableBody, 
        IconButton, Dialog, DialogTitle, DialogContent, DialogActions,
        FormControl, FormLabel, TextField, Select, MenuItem } from '@material-ui/core';

import AddNode from './AddNode/AddNode';
import InfoNode from './InfoNode/InfoNode';

import RefreshIcon from '@material-ui/icons/Refresh';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import InfoIcon from '@material-ui/icons/Info';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';

import Snackbar from '../../hoc/Snackbar/Snackbar';

class NodePage extends Component {
    state = {
        dataResp: [],
        error: false,
        errorResp: {},
        addForm: {
            open: false,
            capacity: '',
            width: '',
            road: '',
            paths: '',
            lanes: '',
        },
        roadData: [],
        info: {
            open: false,
            data: {},
            node: ''
        }
    }

    componentDidMount () {
        console.log("NodePage didMount", this.props)
        this.unlisten = this.props.history.listen((location, action) => {
            console.log("NodePage on route change", location.pathname.split("/"));
            const pattern = /^(\/node)/
            if (pattern.test(location.pathname)) this.getAllDataHandler()
        });
        this.getAllDataHandler()
        if (/^(\/node)\/(\d)*/.test(this.props.location.pathname)) {
            const nodeId = parseInt(this.props.location.pathname.split("/")[2])
            this.infoDataHandler(nodeId)
        }
    }

    componentWillUnmount() {
        console.log("NodePage WillUnmount")
        this.unlisten()
    }
    // componentWillReceiveProps() {
    //     console.log("NodePage willReceiveProps", this.props)
    //     // this.getAllDataHandler()
    // }
    
    addNodeHandler = () => {
        this.setState({ error: false })
        var addData = {}
        addData["th"] = this.state.addForm.lanes
        addData["path"] = this.state.addForm.paths
        addData["width"] = this.state.addForm.width
        addData["capacity"] = this.state.addForm.capacity
        addData["road_id"] = this.state.roadData[this.state.addForm.road].road_id

        axios.post('lane/add', addData)
            .then(res => {
                const lane_id = res.data.lane_id
                axios.post('node/add/' + lane_id)
                    .then(res => {
                        this.setState({
                            addForm: {
                                open: false,
                                capacity: '',
                                width: '',
                                road: '',
                                paths: '',
                                lanes: '',
                            }
                        })
                        this.getAllDataHandler()
                    })
                    .catch(e => {
                        console.log(e.response)
                    })
            })
            .catch(e => {
                console.log(e.response)
                if (e.response) {
                    this.setState({
                        error: true,
                        errorResp: e.response.data
                    })
                }
            })
    }

    addNodeFormHandler = () => {
        const infoUpdate = {
            open: false,
            data: {},
            node: ''
        }
        this.setState({ info: infoUpdate })
        this.props.history.replace('/node/add')
    }

    addNodeChangeHandler = (name, event) => {
        let addFormUpdate = { ...this.state.addForm }
        addFormUpdate[name] = event.target.value
        this.setState({ addForm: addFormUpdate })
    }

    getAllDataHandler = () => {
        console.log("NodePage start getAllDataHandler")
        this.setState({error: false, errorResp: {}})
        axios.get('/node/all')
            .then(res => {
                this.setState({dataResp: res.data.node_data})
                console.log("NodePage start getAllDataHandler")
            })
            .catch(e => {
                this.setState({dataResp: []})
            })
    }

    infoDataHandler = (nodeId) => {
        // console.log("infoDataHandler", nodeId, this.state.info, this.state.info.open, this.state.info.node === nodeId)
        // if (this.state.info.open && this.state.info.node === nodeId) {
        //     const infoUpdate = {
        //         open: false,
        //         data: {},
        //         node: ''
        //     }
        //     this.setState({info: infoUpdate})
        //     this.props.history.replace('/node')
        // } else {
        //     axios.get('/node/' + nodeId)
        //         .then(res => {
        //             const infoUpdate = {
        //                 open: true,
        //                 data: res.data.node_data,
        //                 node: nodeId
        //             }
        //             this.setState({ info: infoUpdate })
        //             console.log(infoUpdate, this.state.info)
        //             this.props.history.replace('/node/' + nodeId)
        //         })
        // }
    }
    
    deleteDataHandler = (node_id) => {
        this.setState({ error: false })
        axios.delete('/node/delete/' + node_id)
            .then(res => {
                console.log(res)
                this.getAllDataHandler()
            })
            .catch(e => {
                if (e.response) {
                    console.log(e.response)
                    this.setState({
                        error: true,
                        errorResp: e.response
                    })
                }
            })
    }

    createConfigNode = (nodeId) => {
        console.log("createConfigNode", nodeId)
        this.setState({ error: false })
        axios.post('/node/create/' + nodeId)
            .then(res => {
                this.getAllDataHandler()
            })
            .catch(e => {
                if (e.response) {
                    console.log(e.response)
                    this.setState({
                        error: true,
                        errorResp: e.response
                    })
                }
            })
    }

    downloadConfigNode = (nodeId) => {
        this.setState({ error: false })
        axios({
            method: 'get',
            url: '/node/download/' + nodeId,
            responseType: 'blob'
        })
            .then(res => {
                let blob = new Blob([res.data], { type: "text/plain;charset=utf-8" });
                saveAs(blob, "config.py")
                this.getAllDataHandler()
            })
            .catch(e => {
                if (e.response) {
                    console.log(e.response)
                    this.setState({
                        error: true,
                        errorResp: e.response
                    })
                }
            })
    }

    render () {
        const { classes } = this.props

        const dataShow = this.state.dataResp.slice()
        let tableData = []

        if (dataShow.length > 0) {
            for (let i = 0; i < dataShow.length; i++) {
                console.log(dataShow[i])
                tableData.push(
                    <TableRow key={i}>
                        <TableCell>{dataShow[i].node_username}</TableCell>
                        <TableCell>{dataShow[i].node_key}</TableCell>
                        {
                            this.props.adminStatus !== 'admin' ?
                                (
                                    <TableCell>
                                        {dataShow[i].node_config === 0 ? (
                                            <Button
                                                variant="contained"
                                                size="small"
                                                className={classes.configIcon}
                                                onClick={() => this.createConfigNode(dataShow[i].node_id)}
                                            >
                                                <NoteAddIcon className={classes.configButton} />
                                                Create
                                </Button>
                                        ) : (
                                                <Button
                                                    variant="contained"
                                                    size="small"
                                                    className={classes.configIcon}
                                                    onClick={() => this.downloadConfigNode(dataShow[i].node_id)}
                                                >
                                                    <CloudDownloadIcon className={classes.configButton} />
                                                    Download
                                </Button>
                                            )}
                                    </TableCell>
                                )
                            : null
                        }
                        <TableCell>
                            {
                                this.props.adminStatus !== 'admin' ?
                                    (
                                        <IconButton className={classes.editDeleteIcon} variant='fab' mini>
                                            <DeleteIcon onClick={() => this.deleteDataHandler(dataShow[i].node_id)} />
                                        </IconButton>
                                    )
                                : null
                            }
                            <IconButton className={classes.editDeleteIcon} variant='fab' mini>
                                <InfoIcon onClick={() => this.props.history.push('/node/info/' + dataShow[i].node_id)} />
                            </IconButton>
                        </TableCell>
                    </TableRow>
                )
            }
        }

        let roadNameItem = []
        let pathsItem = []
        let laneItem = []

        console.log(this.state.roadData.length, "render", this.state.roadData)

        if (this.state.roadData.length > 0) {
            console.log("road data exists", this.state.roadData)
            const roadData = this.state.roadData.slice()

            for (let i = 0; i < roadData.length; i++) {
                const noNode = roadData[i].lanes.slice().filter(lane => {
                    console.log(lane.node, lane.node === null)
                    if (lane.node === null) {}
                })
                const usedNode = roadData[i].lanes.slice().filter(lane => lane.node !== null)
                // console.log(noNode.length < roadData[i].road_lanes, noNode, roadData[i].road_lanes, roadData[i].road_name)
                if (!(usedNode.length >= roadData[i].road_lanes)) {
                    roadNameItem.push(
                        <MenuItem key={i} value={i}>{"Jln " + roadData[i].road_name}</MenuItem>
                    )

                    if (this.state.addForm.road !== '' && this.state.addForm.road === i) {
                        if (roadData[i].road_paths === 2) {
                            pathsItem = [
                                <MenuItem key={0} value={0}><ArrowForwardIcon /></MenuItem>,
                                <MenuItem key={1} value={1}><ArrowBackIcon /></MenuItem>
                            ]
                        } else {
                            pathsItem = [
                                <MenuItem key={0} value={0}><ArrowForwardIcon /></MenuItem>
                            ]
                        }

                        let laneShould = [0,1,2,3]
                        if (roadData[i].road_lanes === 2) {
                            laneShould = [0,1]
                        }

                        console.log(this.state.addForm.paths)
                        if (this.state.addForm.paths !== '') {
                            console.log(this.state.addForm.paths)
                            if (this.state.addForm.paths === 0) {
                                laneShould = laneShould.slice(0,(laneShould.length/roadData[i].road_paths))
                            } else if (this.state.addForm.paths === 1) {
                                laneShould = laneShould.slice((laneShould.length / roadData[i].road_paths))
                            }
                            console.log("laneShould usedNode",laneShould, usedNode)
                            let laneRoad = usedNode.map(lane => lane.lane_th)
                            // const laneRoad = roadData[i].lanes.slice().map(lane => lane.lane_th)
                            // const laneRoad = roadData[i].lanes.slice().filter(l => laneShould.indexOf(l.lane_th) > -1)
                            let unusedLane = laneShould.filter(l => laneRoad.indexOf(l) <= -1)
                            console.log("unusedLane laneRoad", laneRoad, unusedLane)
                            for (let j = 0; j < unusedLane.length; j++) {
                                laneItem.push(
                                    <MenuItem key={j} value={unusedLane[j]}>{unusedLane[j]}</MenuItem>
                                )
                            }
                        }
                    }
                }
            }
        }
        
        console.log(this.state.addForm, laneItem)

        return (
            <div className={classes.root}>
                <Paper className={classes.tablePaper} elevation={1}>
                    <div className={classes.tablePaperHeader}>
                        <Typography variant="title">
                            Node Data Table
                        </Typography>`
                        <Button variant='fab' mini>
                            <RefreshIcon onClick={this.getAllDataHandler} />
                        </Button>
                        {
                            this.props.adminStatus !== 'admin' ?
                                (
                                    <Button variant='fab' mini>
                                        <AddIcon onClick={this.addNodeFormHandler} />
                                    </Button>
                                )
                            : null
                        }
                    </div>
                    <Divider />
                    <Paper className={classes.tablePaperContent}>
                        <Table>
                            <TableHead className={classes.tableHeader}>
                                <TableRow>
                                    <TableCell>Username</TableCell>
                                    <TableCell>Key</TableCell>
                                    {
                                        this.props.adminStatus !== 'admin' ?
                                            <TableCell>Config File</TableCell>
                                        : null
                                    }
                                    <TableCell style={{ width: 96, margin: 'center' }} >Actions</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody className={classes.tableBody}>
                                {tableData}
                            </TableBody>
                        </Table>
                    </Paper>
                </Paper>

                <Switch>
                    { /* Add Node */
                        this.props.adminStatus !== 'admin' ?
                            <Route
                                path={this.props.match.path + '/add'}
                                component={AddNode}
                            />
                        : null
                    }

                    <Route 
                        path={this.props.match.path + '/info/:nodeId'}
                        component={InfoNode}
                    />
                </Switch>
                {/* {this.state.info.open ? <InfoNode data={{...this.state.info.data}} closeHandler={() => this.infoDataHandler(this.state.info.node)} /> : null} */}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        adminStatus: state.auth.status
    }
}

export default connect(mapStateToProps, null)(withStyles(styles)(NodePage))