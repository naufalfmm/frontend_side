import grey from '@material-ui/core/colors/grey';
import lime from '@material-ui/core/colors/lime';

const styles = theme => ({
    // paper: {
    //     ...theme.mixins.gutters(),
    //     marginTop: theme.spacing.unit * 2,
    //     paddingTop: theme.spacing.unit * 2,
    //     paddingBottom: theme.spacing.unit * 2,
    //     borderTop: '1px solid',
    //     borderTopColor: grey[300]
    // },
    // paperHeader: {
    //     height: 80,
    //     '& h2': {
    //         position: 'relative',
    //         top: '50%',
    //         transform: 'translateY(-50%)',
    //         float: 'left'
    //     },
    // },
    // paperContent: {
    //     display: 'flex',
    //     flexWrap: 'wrap',
    //     flex: 1,
    //     justifyContent: 'space-between',
    //     // alignItems: 'center',
    //     // alignContent: 'space-between',
    //     // height: 'auto',
    //     marginBottom: theme.spacing.unit * 4
    // },
    // content: {
    //     flex: '0 45%',
    //     marginTop: theme.spacing.unit,
    //     width: '40%',
    // },
    // roadOptions: {
    //     display: 'flex',
    //     flexWrap: 'wrap',
    //     marginBottom: theme.spacing.unit * 2
    // },
    // paperButton: {
    //     marginTop: 0,
    //     marginBottom: 0,
    //     overflow: 'auto',
    //     whiteSpace: 'nowrap',
    //     clear: 'both',
    //     '& button': {
    //         marginLeft: theme.spacing.unit,
    //         marginRight: theme.spacing.unit,
    //         float: 'right',
    //         backgroundColor: lime[900],
    //         color: theme.palette.common.white,
    //         '&:hover': {
    //             backgroundColor: lime[800]
    //         },
    //         '&[disabled]': {
    //             backgroundColor: grey[600],
    //             cursor: 'not-allowed'
    //         }
    //     }
    // },
    dialogContent: {
        marginTop: theme.spacing.unit * 3
    }, 
    contentForm: {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    contentSimulation: {
        marginBottom: theme.spacing.unit * 2
    },
    content: {
        flexBasis: '48%',
        flexGrow: 1,
        paddingBottom: theme.spacing.unit * 2,
        '&.right': {
            paddingRight: theme.spacing.unit,
        },
        '&.left': {
            paddingLeft: theme.spacing.unit,
        },
        '&.lane': {
            flexGrow: 0
        }
    },
    dialogActions: {
        margin: theme.spacing.unit * 3,
        '& button': {
            marginLeft: theme.spacing.unit,
            marginRight: theme.spacing.unit,
            backgroundColor: lime[900],
            width: 86,
            color: theme.palette.common.white,
            '&:hover': {
                backgroundColor: lime[800]
            },
            '&[disabled]': {
                backgroundColor: grey[600]
            }
        }
    },
    circular: {
        left: '45%',
        position: 'relative',
        color: lime[900]
    }
})

export default styles