import React, { Component } from 'react';
import axios from '../../../axios-ins';
import { withStyles, Divider, FormControl, FormLabel, 
        TextField, MenuItem, CircularProgress, Button, 
        Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';

import Auxi from '../../../hoc/Auxi/Auxi';

import SegmentSimulation from '../../../containers/SegmentSimulation/SegmentSimulation';
import styles from './AddNode.styles';

class AddNode extends Component {
    state = {
        loading: false,
        error: false,
        data: [],
        form: {
            roadId: {
                value: '',
                valid: false,
                touched: false,
                validate: {
                    require: true
                }
            },
            segmentId: {
                value: '',
                valid: false,
                touched: false,
                validate: {
                    require: true
                }
            },
            pathId: {
                value: '',
                valid: false,
                touched: false,
                validate: {
                    require: true
                }
            },
            direction: {
                value: '',
                valid: false,
                touched: false,
                validate: {
                    require: true
                }
            },
            lane: {
                value: '',
                valid: false,
                touched: false,
                validate: {
                    require: true
                }
            },
            width: {
                value: '',
                valid: false,
                touched: false,
                validate: {
                    require: true,
                    isDecimal: true
                }
            },
            capacity: {
                value: '',
                valid: false,
                touched: false,
                validate: {
                    require: true,
                    isNumber: true
                }
            }
        }
    }

    componentDidMount() {
        console.log("AddNode didMount")
        this.getDataHandler()
    }

    componentDidUpdate() {
        console.log("AddNode didUpdate")
    }

    componentWillUnmount() {
        console.log("AddNode willUnmount")
    }

    getDataHandler = () => {
        console.log("AddNode getDataHandler")
        this.setState({loading: true})
        axios.get('road/all')
            .then(res => {
                let stateUpdate = {...this.state}
                stateUpdate.data = res.data.road_full
                stateUpdate.loading = false
                this.setState(stateUpdate)
            })
            .catch(e => {
                let stateUpdate = {...this.state}
                stateUpdate.data = []
                stateUpdate.error = true
                stateUpdate.loading = false
                this.setState(stateUpdate)
            })
    }

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.require) {
            isValid = value.toString().trim() !== '' && isValid;
        }

        if (rules.isNumber) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isDecimal) {
            const pattern = /^\d*\.?\d*$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    submitHandler = () => {
        console.log("AddNode submitHandler", this.state.form)
        this.setState({ loading: true, error: false })

        let addData = {}
        addData["th"] = parseInt(this.state.form.lane.value)
        addData["width"] = parseFloat(this.state.form.width.value)
        addData["capacity"] = parseInt(this.state.form.capacity.value)
        addData["path_id"] = parseInt(this.state.form.pathId.value)
        console.log("AddNode submitHandler", addData)

        axios.post('node/add', addData)
            .then(() => {
                this.setState({ loading: false })
                this.props.history.push('/node')
            })
            .catch(() => {
                this.setState({ loading: false, error: true })
            })
    }

    changeHandler = (name, event) => {
        event.preventDefault()
        console.log("changeHandler")
        let formUpdate = {
            ...this.state.form,
            [name]: {
                ...this.state.form[name],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.form[name].validate),
                touched: true
            }
        }

        const roadId = this.state.form.roadId.value

        console.log(name, event.target.value, roadId, name === 'roadId', event.target.value !== roadId, roadId !== '')
        if (name === 'roadId' && event.target.value !== roadId && roadId !== '') {
            formUpdate = {
                roadId: {
                    value: event.target.value,
                    valid: this.checkValidity(event.target.value, this.state.form[name].validate),
                    touched: true,
                    validate: {
                        require: true
                    }
                },
                segmentId: {
                    value: '',
                    valid: false,
                    touched: false,
                    validate: {
                        require: true
                    }
                },
                pathId: {
                    value: '',
                    valid: false,
                    touched: false,
                    validate: {
                        require: true
                    }
                },
                direction: {
                    value: '',
                    valid: false,
                    touched: false,
                    validate: {
                        require: true
                    }
                },
                lane: {
                    value: '',
                    valid: false,
                    touched: false,
                    validate: {
                        require: true
                    }
                },
                width: {
                    value: '',
                    valid: false,
                    touched: false,
                    validate: {
                        require: true,
                        isDecimal: true
                    }
                },
                capacity: {
                    value: '',
                    valid: false,
                    touched: false,
                    validate: {
                        require: true,
                        isNumber: true
                    }
                }
            }
        }
        console.log(formUpdate)
        // addFormUpdate[name] = event.target.value
        this.setState({ form: formUpdate })
    }

    formHandler = () => {
        this.props.history.push('/node')
    }

    snackbarHandler = () => {
        this.setState({
            ret: null
        })
    }

    laneClicked = (lane, path, pathId) => {
        console.log("laneClicked", lane, path)
        let formUpdate = {
            ...this.state.form,
            lane: {
                ...this.state.form.lane,
                value: lane,
                valid: this.checkValidity(lane, this.state.form.lane.validate),
                touched: true
            },
            direction: {
                ...this.state.form.direction,
                value: path,
                valid: this.checkValidity(path, this.state.form.direction.validate),
                touched: true
            },
            pathId: {
                ...this.state.form.pathId,
                value: pathId,
                valid: this.checkValidity(pathId, this.state.form.pathId.validate),
                touched: true
            }
            
        }
        this.setState({ form: formUpdate })
    }

    render() {
        const { classes } = this.props

        console.log("AddNode render")
        console.log(this.state)

        // let snackbar = null
        // if (this.state.ret !== null) {
        //     const rets = this.state.ret
        //     console.log(this.state[rets].resp)
        //     snackbar = (
        //         <Snackbar
        //             anchorOrigin={{
        //                 vertical: 'top',
        //                 horizontal: 'center',
        //             }}
        //             variant={rets}
        //             open={this.state.ret !== null}
        //             autoHideDuration={6000}
        //             onClose={this.snackbarHandler}
        //             ContentProps={{
        //                 'aria-describedby': 'message-id',
        //             }}
        //             message={<span id="message-id">{this.state[rets].resp.message}</span>}
        //         />
        //     )
        // }

        let form = (
            <Auxi>
                <CircularProgress className={classes.circular} size={60} />
            </Auxi>
        )

        let actions = null

        let disableButton = (
            !(this.state.form.roadId.valid && this.state.form.roadId.touched) ||
            !(this.state.form.segmentId.valid && this.state.form.segmentId.touched) ||
            !(this.state.form.pathId.valid && this.state.form.pathId.touched) ||
            !(this.state.form.direction.valid && this.state.form.direction.touched) ||
            !(this.state.form.lane.valid && this.state.form.lane.touched) ||
            !(this.state.form.width.valid && this.state.form.width.touched) ||
            !(this.state.form.capacity.valid && this.state.form.capacity.touched)
        )

        if (!this.state.loading) {
            let roadsim = null
            let roadNameItem = []
            let segmentItem = []

            const roadData = this.state.data.slice()
            for (let i=0; i<roadData.length; i++) {
                let segmentsData = roadData[i].segments.slice()
                let roadChoosen = []
                console.log("AddNode segmentsData roadChoosen", i, segmentsData, roadChoosen)
                for (let j=0; j<segmentsData.length; j++) {
                    let pathsData = segmentsData[j].paths.slice()
                    let segmentChoosen = false
                    console.log("AddNode pathsData", j, pathsData)
                    for (let k=0; k<pathsData.length; k++) {
                        if (pathsData[k].path_lanes <= pathsData[k].lanes.length) segmentChoosen = segmentChoosen || false
                        else segmentChoosen = segmentChoosen || true
                        console.log("AddNode path_lanes lanes.length roadChoosen", k, pathsData[k].path_lanes, pathsData[k].lanes.length, roadChoosen)
                    }
                    console.log("AddNode input segment", this.state.form.roadId.value, j, roadChoosen, roadChoosen[j], segmentsData[j].segment_start, segmentsData[j].segment_end)
                    if (segmentChoosen) {
                        roadChoosen.push(true)
                        if (this.state.form.roadId.value !== '' && this.state.form.roadId.value === i) {
                            segmentItem.push(
                                <MenuItem key={j} value={j}>{segmentsData[j].segment_start + " - " + segmentsData[j].segment_end}</MenuItem>
                            )
                        }
                    } else {
                        roadChoosen.push(false)
                    }
                }
                if (roadChoosen.indexOf(true) > -1) {
                    roadNameItem.push(
                        <MenuItem key={i} value={i}>{"Jln. " + roadData[i].road_name}</MenuItem>
                    )
                }
            }

            let simOpt = [{}, {}]
            let laneSim = [0,0]
            if (this.state.form.roadId.value !== '' && this.state.form.segmentId.value !== '') {
                const roadIdx = this.state.form.roadId.value
                const segmentIdx = this.state.form.segmentId.value
                console.log("AddNode roadId segmentId", roadIdx, segmentIdx)
                // let pathCount = roadData[roadIdx].segments[segmentIdx].segment_path
                let pathDataArr = roadData[roadIdx].segments[segmentIdx].paths.slice()
                console.log("AddNode pathDataArr", pathDataArr)
                for (let i=0; i<pathDataArr.length; i++) {
                    const laneData = pathDataArr[i].lanes.slice()
                    const laneCount = pathDataArr[i].path_lanes
                    console.log("AddNode laneData laneCount", laneData, laneCount)
                    laneSim[i] = laneCount
                    const recordedLane = laneData.map(lane => lane.lane_th)
                    console.log("AddNode laneSim recordedLane", laneSim, recordedLane)
                    let opt = {}
                    for (let j=0; j<laneCount; j++) {
                        console.log(j, recordedLane.indexOf(j))
                        if (recordedLane.indexOf(j) > -1) {
                            opt[j] = {
                                enable: false
                            }
                        } else {
                            opt[j] = {
                                enable: true,
                                clickHandler: this.laneClicked,
                                pathId: pathDataArr[i].path_id
                            }
                        }
                    }
                    console.log("AddNode opt", opt)
                    simOpt[i] = {...opt}
                    console.log("AddNode simOpt", simOpt)
                }
            }

            if (this.state.form.direction.value !== '' && this.state.form.lane.value !== '') {
                const dirIdx = this.state.form.direction.value
                const laneIdx = this.state.form.lane.value

                simOpt[dirIdx][laneIdx] = {
                    active: true
                }
            }

            // if (this.state.form.roadId.value !== '') {
            //     const id = this.state.form.roadId.value
            //     let laneOpt = {}
            //     let laneShould = [0, 1, 2, 3]
            //     console.log(roadData, id, roadData[id])
            //     if (roadData[id].road_lanes === 2) {
            //         laneShould = [0, 1]
            //     }
            //     const laneData = roadData[id].lanes.slice()
            //     const recordedLane = laneData.map(lane => lane.lane_th)
            //     const usedLane = laneShould.filter(l => recordedLane.indexOf(l) > -1)
            //     const laneChoosen = this.state.form.lane.value
            //     console.log(usedLane)
            //     for (let i=0; i<laneShould.length; i++) {
            //         console.log(i, usedLane, usedLane.indexOf(i))
            //         if (usedLane.indexOf(i) > -1) {
            //             laneOpt[i] = {
            //                 disabled: true
            //             }
            //         } else {
            //             laneOpt[i] = {
            //                 active: (laneChoosen === i),
            //                 enable: !(laneChoosen === i)
            //             }
            //         }
            //     }
            //     console.log("laneOpt", laneOpt)
            //     roadsim = <RoadSim paths={roadData[id].road_paths} lanes={roadData[id].road_lanes} opt={{...laneOpt}} clicked={(lane, path) => this.laneClicked(lane, path)} />
            // }

            console.log("AddNode", simOpt, simOpt.length)

            form = (
                <Auxi>
                    <div className={classes.contentForm}>
                        <FormControl required className={[classes.content, "right"].join(' ')}>
                            <FormLabel component="legend">Road Name</FormLabel>
                            <TextField
                                select
                                required
                                error={!this.state.form.roadId.valid && this.state.form.roadId.touched}
                                id="roadId"
                                name="roadId"
                                value={this.state.form.roadId.value}
                                onChange={(event) => this.changeHandler('roadId', event)}
                            >
                                <MenuItem value="" />
                                {roadNameItem.slice()}
                            </TextField>
                        </FormControl>
                        <FormControl required className={[classes.content, "left"].join(' ')}>
                            <FormLabel component="legend">Segment</FormLabel>
                            <TextField
                                select
                                required
                                error={!this.state.form.segmentId.valid && this.state.form.segmentId.touched}
                                id="segmentId"
                                name="segmentId"
                                value={this.state.form.segmentId.value}
                                onChange={(event) => this.changeHandler('segmentId', event)}
                            >
                                <MenuItem value="" />
                                {segmentItem.slice()}
                            </TextField>
                        </FormControl>
                    </div>
                    <div className={classes.contentSimulation}>
                        <SegmentSimulation lane={laneSim.slice()} opt={simOpt.slice()} />
                    </div>
                    <div className={classes.contentForm}>
                        <FormControl disabled className={[classes.content, "right"].join(' ')}>
                            <FormLabel component="legend">Direction</FormLabel>
                            <TextField
                                disabled
                                error={!this.state.form.direction.valid && this.state.form.direction.touched}
                                id="direction"
                                name="direction"
                                value={this.state.form.direction.value}
                            />
                        </FormControl>
                        <FormControl disabled className={[classes.content, "left"].join(' ')}>
                            <FormLabel component="legend">Lane</FormLabel>
                            <TextField
                                disabled
                                error={!this.state.form.lane.valid && this.state.form.lane.touched}
                                id="lane"
                                name="lane"
                                value={this.state.form.lane.value}
                            />
                        </FormControl>
                        <FormControl required className={[classes.content, "right"].join(' ')}>
                            <FormLabel component="legend">Lane Width</FormLabel>
                            <TextField
                                required
                                error={!this.state.form.width.valid && this.state.form.width.touched}
                                id="width"
                                name="width"
                                value={this.state.form.width.value}
                                onChange={(event) => this.changeHandler('width', event)}
                            />
                        </FormControl>
                        <FormControl required className={[classes.content, "left"].join(' ')}>
                            <FormLabel component="legend">Lane Capacity</FormLabel>
                            <TextField
                                required
                                error={!this.state.form.capacity.valid && this.state.form.capacity.touched}
                                id="capacity"
                                name="capacity"
                                value={this.state.form.capacity.value}
                                onChange={(event) => this.changeHandler('capacity', event)}
                            />
                        </FormControl>
                    </div>
                </Auxi>
            )

            actions = (
                <Auxi>
                    <Divider />
                    <DialogActions className={classes.dialogActions}>
                        <Button
                            color="primary"
                            disabled={disableButton}
                            onClick={this.submitHandler}>
                            Add
                        </Button>
                        <Button color="primary" onClick={this.formHandler}>Cancel</Button>
                    </DialogActions>
                </Auxi>
            )
        }
        // console.log('return', this.state.ret)
        return (
            <Dialog
                onClose={this.formHandler}
                open
            >
                <DialogTitle>{"Add Node"}</DialogTitle>
                <Divider />
                <DialogContent className={classes.dialogContent}>
                    {form}
                </DialogContent>
                {actions}
            </Dialog>
        )
    }
}

export default withStyles(styles)(AddNode)