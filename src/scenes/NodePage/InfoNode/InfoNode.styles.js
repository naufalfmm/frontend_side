import grey from '@material-ui/core/colors/grey';
import lime from '@material-ui/core/colors/lime';

const styles = theme => ({
    // paper: {
    //     ...theme.mixins.gutters(),
    //     marginTop: theme.spacing.unit * 2,
    //     paddingTop: theme.spacing.unit * 2,
    //     paddingBottom: theme.spacing.unit * 2,
    //     borderTop: '1px solid',
    //     borderTopColor: grey[300]
    // },
    // paperHeader: {
    //     height: 80,
    //     '& h2': {
    //         position: 'relative',
    //         top: '50%',
    //         transform: 'translateY(-50%)',
    //         float: 'left'
    //     },
    // },
    // paperContent: {
    //     display: 'flex',
    //     flexWrap: 'wrap',
    //     flex: 1,
    //     justifyContent: 'space-between',
    //     // alignItems: 'center',
    //     // alignContent: 'space-between',
    //     // height: 'auto',
    //     marginBottom: theme.spacing.unit * 4
    // },
    // roadsimDiv: {
    //     marginTop: theme.spacing.unit
    // },
    // contentDiv: {
    //     flex: '0 45%',
    //     marginTop: theme.spacing.unit,
    //     width: '40%',
    // },
    // content: {
    //     width: '100%',
    //     marginTop: theme.spacing.unit
    // },
    // paperButton: {
    //     marginTop: 0,
    //     marginBottom: 0,
    //     overflow: 'auto',
    //     whiteSpace: 'nowrap',
    //     clear: 'both',
    //     '& button': {
    //         marginLeft: theme.spacing.unit,
    //         marginRight: theme.spacing.unit,
    //         float: 'right',
    //         backgroundColor: lime[900],
    //         color: theme.palette.common.white,
    //         '&:hover': {
    //             backgroundColor: lime[800]
    //         },
    //         '&[disabled]': {
    //             backgroundColor: grey[600],
    //             cursor: 'not-allowed'
    //         }
    //     }
    // },
    dialogContent: {
        marginTop: theme.spacing.unit * 3
    },
    properties: {
        display: 'flex',
        flexDirection: 'row'
    },
    laneProperties: {
        flexBasis: '45%',
        marginRight: theme.spacing.unit,
        '& p': {
            fontSize: '0.9rem',
            marginBottom: theme.spacing.unit/2
        }
    },
    nodeProperties: {
        flexBasis: '45%',
        marginLeft: theme.spacing.unit,
        '& p': {
            fontSize: '0.9rem',
            marginBottom: theme.spacing.unit/2
        }
    },
    configIcon: {
        textTransform: 'none',
        backgroundColor: lime[900],
        color: theme.palette.common.white,
        '&:hover': {
            backgroundColor: lime[800]
        }
    },
    segmentSimulation: {
        marginTop: theme.spacing.unit * 2,
    },
    circular: {
        left: '35%',
        position: 'relative',
        color: lime[900]
    }
})

export default styles