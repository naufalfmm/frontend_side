import React, { Component } from 'react';
import axios from '../../../axios-ins';
import { withStyles, Divider, Typography, Paper,
        FormControl, FormLabel, TextField, Button, Dialog, DialogTitle, DialogContent, CircularProgress } from '@material-ui/core';
import { saveAs } from 'file-saver/FileSaver';
import styles from './InfoNode.styles';

import RoadSim from '../../../containers/SegmentSimulation/SegmentSimulation';
import Auxi from '../../../hoc/Auxi/Auxi';
import SegmentSimulation from '../../../containers/SegmentSimulation/SegmentSimulation';

import NoteAddIcon from '@material-ui/icons/NoteAdd';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';

class InfoNode extends Component {
    state = {
        loading: false,
        error: false,
        data: {},
        segmentData: {}
    }

    componentDidMount() {
        console.log("InfoNode didMount", this.props.match.params.nodeId)
        this.getDataHandler()
    }

    componentDidUpdate(prevProps, prevState) {
        // console.log("InfoNode didUpdate openAdd", prevState.openAdd, this.state.openAdd)
        if (prevProps.match.params.nodeId !== this.props.match.params.nodeId) {
            console.log("InfoNode didUpdate", prevProps.match.params.nodeId, this.props.match.params.nodeId)
            this.getDataHandler()
        }
    }

    getDataHandler = () => {
        this.setState({ loading: true })
        console.log("InfoNode getDataHandler", this.props.match.params.nodeId)
        axios.get('node/' + this.props.match.params.nodeId)
            .then(res => {
                let stateUpdate = {
                    loading: false,
                    error: false,
                    data: res.data.node_data,
                    segmentData: res.data.segment_data
                }
                this.setState(stateUpdate)
            })
            .catch(() => {
                let stateUpdate = {
                    loading: false,
                    error: true,
                    data: {},
                    segmentData: {}
                }
                this.setState(stateUpdate)
            })
    }

    createConfigNode = (nodeId) => {
        console.log("createConfigNode", nodeId)
        this.setState({ error: false })
        axios.post('/node/create/' + nodeId)
            .then(res => {
                this.getDataHandler()
            })
            .catch(e => {
                this.setState({ error: true })
            })
    }

    downloadConfigNode = (nodeId) => {
        this.setState({ error: false })
        axios({
            method: 'get',
            url: '/node/download/' + nodeId,
            responseType: 'blob'
        })
            .then(res => {
                let blob = new Blob([res.data], { type: "text/plain;charset=utf-8" });
                saveAs(blob, "config.py")
                this.getDataHandler()
            })
            .catch(e => {
                this.setState({ error: true })
            })
    }

    render() {
        const { classes } = this.props

        let form = (
            <Auxi>
                <CircularProgress className={classes.circular} size={60} />
            </Auxi>
        )

        const infoData = { ...this.state.data }
        if (!this.state.loading && infoData.node_id !== undefined) {
            console.log("InfoNode render", infoData, this.state.data)
            const segmentData = {...this.state.segmentData}
            const pathsCount = infoData.lane.path.segment.segment_paths
            const laneCount = infoData.lane.path.path_lanes

            const laneNode = infoData.lane.lane_th
            const pathNode = infoData.lane.path.path_th

            let laneSim = [0,0]
            const pathsData = segmentData.paths.slice()
            console.log("InfoNode render pathsData", pathsData, pathsData.length)
            for (let i=0; i<pathsData.length; i++) {
                console.log("InfoNode render inFor", pathsData[i], pathsData[i].path_th, pathsData[i].path_lanes)
                laneSim[pathsData[i].path_th] = pathsData[i].path_lanes
            }

            let simOpt = [{}, {}]
            for (let i=0; i<pathsCount; i++) {
                let opt = {}
                for (let j=0; j<laneSim[i]; j++) {
                    if (i === pathNode && j === laneNode) {
                        opt[j] = {
                            hidden: true
                        }
                    } else {
                        opt[j] = {
                            hidden: false
                        }
                    }
                }
                simOpt[i] = {...opt}
            }

            console.log("InfoNode render laneSim simOpt", laneSim, simOpt)

            form = (
                <Auxi>
                    <div className={classes.properties}>
                        <div className={classes.laneProperties}>
                            <Typography style={{ fontWeight: 'bold', fontSize: '1.1rem', paddingBottom: 8 }}>Lane Properties</Typography>
                            <Typography>{"Road Name: Jln. " + infoData.lane.path.segment.road.road_name}</Typography>
                            <Typography>{"Segment: " + infoData.lane.path.segment.segment_start + " - " + infoData.lane.path.segment.segment_end}</Typography>
                            <Typography>{"Path/Lane: " + infoData.lane.path.path_th + "/" + infoData.lane.lane_th}</Typography>
                            <Typography>{"Lane Width: " + infoData.lane.lane_width + " m"}</Typography>
                            <Typography>{"Lane Capacity: " + infoData.lane.lane_cap}</Typography>
                        </div>
                        <div className={classes.nodeProperties}>
                            <Typography style={{ fontWeight: 'bold', fontSize: '1.1rem', paddingBottom: 8 }}>Node Properties</Typography>
                            <Typography>{"Username: " + infoData.node_username}</Typography>
                            <Typography style={{wordBreak: 'break-all'}}>{"Key: " + infoData.node_key}</Typography>
                            <div className={classes.configFile}>
                                <Typography>{"Config File: "}</Typography>
                                {infoData.node_config === 0 ? (
                                    <Button
                                        variant="contained"
                                        size="small"
                                        className={classes.configIcon}
                                        onClick={() => this.createConfigNode(infoData.node_id)}
                                    >
                                        <NoteAddIcon className={classes.configButton} />
                                        Create
                                </Button>
                                ) : (
                                        <Button
                                            variant="contained"
                                            size="small"
                                            className={classes.configIcon}
                                            onClick={() => this.downloadConfigNode(infoData.node_id)}
                                        >
                                            <CloudDownloadIcon className={classes.configButton} />
                                            Download
                                </Button>
                                    )}
                            </div>
                        </div>
                    </div>
                    
                    <div className={classes.segmentSimulation}>
                        <Typography style={{ fontWeight: 'bold', fontSize: '1.1rem', paddingBottom: 8 }}>Node Position</Typography>
                        <SegmentSimulation lane={laneSim.slice()} opt={simOpt.slice()} />
                    </div>
                </Auxi>
            )
        }

        return (
            <Dialog
                onClose={() => this.props.history.push('/node')}
                open
            >
                <DialogTitle>{"Info Node"}</DialogTitle>
                <Divider />
                <DialogContent className={classes.dialogContent}>
                    {form}
                </DialogContent>
            </Dialog>
        )
    }
}

export default withStyles(styles)(InfoNode)