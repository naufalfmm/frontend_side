import lime from '@material-ui/core/colors/lime';
import grey from '@material-ui/core/colors/grey';

const styles = theme => ({
    root: {
        width: '100%',
        height: '100%',
    },
    tablePaper: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    tablePaperHeader: {
        height: 80,
        '& h2': {
            position: 'relative',
            top: '50%',
            transform: 'translateY(-50%)',
            float: 'left'
        },
        '& button': {
            position: 'relative',
            marginLeft: theme.spacing.unit,
            marginRight: theme.spacing.unit,
            top: '50%',
            transform: 'translateY(-50%)',
            float: 'right',
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            '&:hover': {
                backgroundColor: lime[800]
            }
        }
    }, 
    tableHeader: {
        '& th': {
            backgroundColor: lime[900],
            color: theme.palette.common.white,
            textAlign: 'center',
            fontSize: '0.8rem'
        }
    },
    tablePaperContent: {
        marginTop: theme.spacing.unit * 2,
        overflow: 'auto'
    },
    tableBody: {
        '& td': {
            textAlign: 'center',
            fontSize: '0.8rem'
        }
    },
    editDeleteIcon: {
        float: 'right',
    },
    configButton: {
        marginRight: theme.spacing.unit
    },
    configIcon: {
        textTransform: 'none',
        backgroundColor: lime[900],
        color: theme.palette.common.white,
        '&:hover': {
            backgroundColor: lime[800]
        }
    },
    laneOptions: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        justifyContent: 'space-between',
        width: '95%',
        alignItems: 'center',
        alignContent: 'space-between',
        height: 'auto',
        marginBottom: theme.spacing.unit * 4
    },
    contentOfLaneOptions: {
        flex: '0 48%',
        marginTop: theme.spacing.unit,
        width: '40%'
        // paddingRight: theme.spacing.unit
    },
    pathsOptions: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        justifyContent: 'space-between',
        width: '95%',
        alignItems: 'center',
        alignContent: 'space-between',
        height: 'auto',
        marginBottom: theme.spacing.unit * 4
    },
    contentOfPathsOptions: {
        flex: '0 48%',
        marginTop: theme.spacing.unit,
        width: '40%'
        // paddingRight: theme.spacing.unit
    },
    roadOptions: {
        width: '100%',
        marginBottom: theme.spacing.unit * 2
    },
    contentOfRoadOptions: {
        // alignItems: 'center'
        left: '50%',
        transform: 'translateX(-50%)',
    },
    laneGroup: {
        marginTop: theme.spacing.unit * 2,
        width: '100%',
        height: '100px',
        overflow: 'hidden',
    },
    lane: {
        width: 'auto',
        height: '17%',
        backgroundColor: grey[800],
        color: grey[50],
        borderStyle: 'solid',
        borderWidth: '1px',
        borderColor: lime[900],
        textAlign: 'center'
    }
})

export default styles