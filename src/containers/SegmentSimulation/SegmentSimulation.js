import React from 'react';
import { withStyles, Typography } from '@material-ui/core';

import styles from './SegmentSimulation.styles';
import Auxi from '../../hoc/Auxi/Auxi';

import ArrowBack from '@material-ui/icons/ArrowBack';
import ArrowForward from '@material-ui/icons/ArrowForward';

/*
lane: [a,b] //if path === 1, [a,0]
opt: [ (optional)
    {
        lane1: {
            enable:, //if true, able to clicked (enable)
            clickHandler, (function)
            pathId: , //if enable
            active:, //if clicked
            hidden:
        },
        lane2: {
            enable:, //if true, able to clicked (enable)
            clickHandler, (function)
            pathId: , //if enable
            active:,
            hidden:
        },
        ...
    },
    {
        ...
    },
    ...
]
*/
const segmentSimulation = (props) => {
    const { classes } = props

    let road = []
    const icons = [
        <ArrowForward className={classes.icon} />,
        <ArrowBack className={classes.icon} />
    ]

    console.log("SegmentSimulation", props.opt)
    let roadOpt = []
    if (props.opt !== undefined) {
        roadOpt = props.opt.slice()
    }

    let addition = 0
    for (let i=0; i<props.lane.length; i++) {
        if (i > 0 && props.lane[i] > 0) road.push(<div key={props.lane[0] + props.lane[1]} className={classes.roadblock} />)
        for (let j=0; j<props.lane[i]; j++) {
            console.log(i + addition)

            let roadClass = [classes.road]
            let iconGroupClass = [classes.iconGroup]
            let pClass = [classes.p]

            if (roadOpt.length > 0) {
                let optPath = {...roadOpt[i]}
                if (!optPath[j].enable) {
                    roadClass.push('disabled')
                    iconGroupClass.push('disabled')
                    pClass.push('disabled')
                } else {
                    roadClass.push('enable')
                    iconGroupClass.push('enable')
                }
                if (optPath[j].active) {
                    roadClass.push('active')
                }
                if (optPath[j].hidden) {
                    roadClass.push('hidden')
                }

                if (!optPath[j].enable) {
                    road.push(
                        <div key={j+addition} className={roadClass.slice().join(' ')}>
                            <div className={iconGroupClass.slice().join(' ')}>
                                <Typography className={pClass.slice().join(' ')}>{'(Lane: ' + j.toString() + ')'}</Typography>{icons[i]}
                            </div>
                        </div>
                    )
                } else {
                    road.push(
                        <div key={j+addition} className={roadClass.slice().join(' ')}>
                            <div className={iconGroupClass.slice().join(' ')} onClick={() => optPath[j].clickHandler(j, i, optPath[j].pathId)}>
                                <Typography className={pClass.slice().join(' ')}>{'(Lane: ' + j.toString() + ')'}</Typography>{icons[i]}
                            </div>
                        </div>
                    )
                }
            } else {
                road.push(
                    <div key={j+addition} className={classes.road}>
                        <div className={classes.iconGroup}>
                            <Typography className={classes.p}>{'(Lane: ' + j.toString() + ')'}</Typography>{icons[i]}
                        </div>
                    </div>
                )
            }
        }
        addition = props.lane[i]
    }

    return (
        <div className={classes.roadGroup}>
            {road}
        </div>
    )
}

export default withStyles(styles)(segmentSimulation)