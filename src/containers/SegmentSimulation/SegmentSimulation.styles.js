import grey from '@material-ui/core/colors/grey';
import blueGrey from '@material-ui/core/colors/blueGrey';

const styles = theme => ({
    road: {
        backgroundColor: grey[900],
        height: 35,
        width: '80%',
        borderBottom: '2px solid white',
        '&.enable': {
            '&:hover': {
                backgroundColor: blueGrey[500],
                cursor: 'pointer'
            },
            '&:clicked': {
                backgroundColor: grey[700]
            }
        },
        '&.disabled': {
            backgroundColor: '#380808',
            cursor: 'not-allowed'
        },
        '&.active': {
            backgroundColor: blueGrey[500]
        },
        '&.hidden': {
            backgroundColor: grey[300]
        }
    },
    roadblock: {
        backgroundColor: 'white',
        height: 15,
        width: '80%',
    },
    iconGroup: {
        top: '50%',
        color: 'white',
        textColor: 'white',
        position: 'relative',
        transform: 'translateY(-50%)',
        textAlign: 'center',
        '&.disabled': {
            color: '#380808'
        }
    },
    roadGroup: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    icon: {
        top: '50%',
        transform: 'translateY(-50%)',
        position: 'absolute',
        fontSize: '1.3rem'
    },
    p: {
        display: 'inline',
        fontSize: '0.8rem',
        color: 'white',
        marginRight: theme.spacing.unit,
        '&.disabled': {
            color: '#380808'
        }
    }
})

export default styles