import React from 'react';
import { NavLink } from 'react-router-dom';
import Auxi from '../../../hoc/Auxi/Auxi';
import { Divider, ListItem, ListItemIcon, ListItemText, withStyles } from '@material-ui/core';

import styles from './NavLinkDrawer.styles';

const navLinkDrawer = (props) => {
    const { classes } = props

    return (
        <Auxi>
            <NavLink to={props.to} className={classes.link}>
                <ListItem button>
                    <ListItemIcon>
                        {props.icon}
                    </ListItemIcon>
                    <ListItemText primary={props.text} />
                </ListItem>
            </NavLink>
            <Divider />
        </Auxi>
    )
}

export default withStyles(styles)(navLinkDrawer)