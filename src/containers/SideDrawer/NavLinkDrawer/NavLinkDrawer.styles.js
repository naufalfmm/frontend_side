const styles = theme => ({
    link: {
        '&:active': {
            color: '#BDBDBD'
        },
        '&:hover': {
            color: '#BDBDBD'
        }
    }
})

export default styles