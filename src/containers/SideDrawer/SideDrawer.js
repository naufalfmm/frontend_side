import React from 'react';
import { connect } from 'react-redux';
import { Drawer, List, ListItem, ListItemText, 
        withStyles, Divider, ListItemIcon } from '@material-ui/core';
import { NavLink } from 'react-router-dom';

import FaceIcon from '@material-ui/icons/Face';
import HomeIcon from '@material-ui/icons/Home';
import DeviceIcon from '@material-ui/icons/Devices';
import TrafficIcon from '@material-ui/icons/Traffic';
import ViewStreamIcon from '@material-ui/icons/ViewStream';

import styles from './SideDrawer.styles';
import NavLinkDrawer from './NavLinkDrawer/NavLinkDrawer';
import * as actions from '../../store/actions/auth';

const sideDrawer = (props) => {

    const linkComp = [
        { 
            to: '/',
            icon: <HomeIcon />,
            text: 'Home'
        },
        { 
            to: '/admin',
            icon: <FaceIcon />,
            text: 'Admin'
        },
        {
            to: '/road',
            icon: <ViewStreamIcon />,
            text: 'Road'
        },
        { 
            to: '/node',
            icon: <DeviceIcon />,
            text: 'Node'
        },
        {
            to: '/traffic',
            icon: <TrafficIcon />,
            text: 'Traffic Data'
        }
    ]

    let navLink = []

    console.log("SideDrawer", props)

    for (let i=0; i<linkComp.length; i++) {
        if (linkComp[i].text === 'Admin') {
            if (props.adminStatus === 'superadmin' || props.adminStatus === 'masteradmin') {
                navLink.push(
                    <NavLinkDrawer key={i} to={linkComp[i].to} icon={linkComp[i].icon} text={linkComp[i].text} />
                )
            }
        } else {
            navLink.push(
                <NavLinkDrawer key={i} to={linkComp[i].to} icon={linkComp[i].icon} text={linkComp[i].text} />
            )
        }
    }

    const classes = props.classes
    return (
        <Drawer open={props.openDrawer} onClose={props.toggleDrawer}>
            <div 
                tabIndex={0} 
                role="button" 
                onClick={props.toggleDrawer} 
                onKeyDown={props.toggleDrawer}
            >
                <List component="nav" className={classes.List}>
                    {navLink}
                    {/* <NavLink to="/" activeStyle={{ color: "#BDBDBD" }} >
                        <ListItem button>
                            <ListItemIcon>
                                <HomeIcon />
                            </ListItemIcon>
                            <ListItemText primary="Home" />
                        </ListItem>
                    </NavLink>
                    <Divider />
                    <ListItem button>
                        <ListItemIcon>
                            <FaceIcon />
                        </ListItemIcon>
                        <ListItemText primary="Admin" />
                    </ListItem>
                    <Divider />
                    <ListItem button>
                        <ListItemIcon>
                            <ViewStreamIcon />
                        </ListItemIcon>
                        <ListItemText primary="Road" />
                    </ListItem>
                    <Divider />
                    <ListItem button>
                        <ListItemIcon>
                            <DeviceIcon />
                        </ListItemIcon>
                        <ListItemText primary="Node" />
                    </ListItem>
                    <Divider />
                    <ListItem button>
                        <ListItemIcon>
                            <TrafficIcon />
                        </ListItemIcon>
                        <ListItemText primary="Traffic Data" />
                    </ListItem> */}
                </List>
            </div>
        </Drawer>
    )
}

const mapStateToProps = state => {
    return {
        adminStatus: state.auth.status
    }
}


export default connect(mapStateToProps, null)(withStyles(styles)(sideDrawer))