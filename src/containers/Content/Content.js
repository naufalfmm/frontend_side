import React from 'react';
import { withStyles } from '../../../node_modules/@material-ui/core';

import styles from './Content.styles';

// import RoadPage from '../../scenes/RoadPage/RoadPage';
// import AdminPage from '../../scenes/AdminPage/AdminPage';
// import NodePage from '../../scenes/NodePage/NodePage';
// import TrafficDataPage from '../../scenes/TrafficDataPage/TrafficDataPage';
// import HomePage from '../../scenes/HomePage/HomePage'

const content = (props) => {
    const { classes } = props

    return (
        <div className={classes.root}>
            {/* <AdminPage /> */}
            {/* <RoadPage /> */}
            {/* <NodePage /> */}
            {/* <TrafficDataPage /> */}
            {/* <HomePage /> */}
        </div>        
    )
}

export default withStyles(styles)(content)