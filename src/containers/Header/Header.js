import React from 'react';
import { AppBar, Toolbar, IconButton, withStyles, 
        Typography, Button, Menu, MenuItem } from '@material-ui/core';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

import styles from './Header.styles';

const header = (props) => {
    const { classes } = props

    console.log("Header", props, props.anchorEl)
    
    return (
        <AppBar className={classes.root} position="fixed">
            <Toolbar>
                <IconButton className={classes.menuButton} color="inherit" aria-label="menu" onClick={props.toggleDrawer}>
                    <MenuIcon />
                </IconButton>
                <Typography variant="title" color="inherit" className={classes.title}>
                    Congestion Detection System
                </Typography>
                <div>
                <Button
                    aria-owns={props.anchorEl ? 'simple-menu' : null}
                    aria-haspopup="true"
                    className={classes.userButton} 
                    color="inherit"
                    onClick={(event) => props.toggleMenu(event)}
                >
                    <AccountCircleIcon className={classes.userIcon} />
                    {props.username}
                </Button>
                <Menu
                    id="simple-menu"
                    anchorEl={props.anchorEl}
                    open={Boolean(props.anchorEl)}
                    onClose={(event) => props.toggleMenu(event)}
                >
                    <MenuItem className={classes.menuItem}>
                        <Link to="/logout">
                            Logout
                        </Link>
                    </MenuItem>
                </Menu>
                </div>
                {/* <IconButton color="inherit" aria-label="menu">
                        <AccountCircleIcon />
                    </IconButton> */}
            </Toolbar>
        </AppBar>
    )
}

const mapStateToProps = state => {
    return {
        username: state.auth.username
    }
}

export default connect(mapStateToProps,null)(withStyles(styles)(header))