import lime from '@material-ui/core/colors/lime';

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: lime[900]
    },
    title: {
        flex: 1
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    userButton: {
        textTransform: 'none'
    },
    userIcon: {
        marginRight: theme.spacing.unit
    },
    menuItem: {
        '& a': {
            textDecoration: 'none'
        }
    }
})

export default styles