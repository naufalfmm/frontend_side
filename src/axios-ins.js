import axios from 'axios';

let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cGVyX2FkbWluIiwic3RhdHVzIjoic3VwZXJhZG1pbiIsImlhdCI6MTUzMzcyMzMxNCwiZXhwIjoxNTM1NTIzMzE0fQ.EQTFPJX5jA-1i-xPZxduuzV1S0BilPbI2j9bFuW6eZI'

const instance = axios.create({
    baseURL: 'http://localhost:7777/api/',
    headers: {
        'auth': token,
        'Content-Type': 'application/json'
    }
})

export const setToken = (tokenImp) => {
    token = tokenImp
    instance.defaults.headers['auth'] = tokenImp
    console.log("setToken", token, instance)
}

export default instance