import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { connect } from "react-redux";
import * as actions from './store/actions/auth';
import axios from './axios-ins';

import Layout from './hoc/Layout/Layout';
import LoginPage from './scenes/LoginPage/LoginPage';
import Logout from './components/Logout/Logout';
import RoadPage from './scenes/RoadPage/RoadPage';
import AdminPage from './scenes/AdminPage/AdminPage';
import NodePage from './scenes/NodePage/NodePage';
import TrafficDataPage from './scenes/TrafficDataPage/TrafficDataPage';
import HomePage from './scenes/HomePage/HomePage';

import Snackbar from './hoc/Snackbar/Snackbar';

class App extends Component {

  componentDidMount () {
    console.log("App didMount", this.props.location.pathname)
    this.props.onTryAutoSignup(this.props.location.pathname);
  }

  render() {
    console.log("App render", this.props.isAuthenticated, this.props.adminStatus, this.props)

    let routes = (
      <Switch>
        <Route path="/login" component={LoginPage} />
        <Redirect to="/login" />
      </Switch>
    )

    if (this.props.isAuthenticated) {
      if (this.props.adminStatus === 'superadmin' || this.props.adminStatus === 'masteradmin') {
        routes = (
          <Switch>
            <Layout>
              <Switch>
                <Route path="/admin" component={AdminPage} />
                <Route path="/login" component={LoginPage} />
                <Route path="/logout" component={Logout} />
                <Route path="/traffic" component={TrafficDataPage} />
                <Route path="/node" component={NodePage} />
                <Route path="/road" component={RoadPage} />
                <Route path="/:sevStat" component={HomePage} />
                <Route path="/" exact component={HomePage} />
              </Switch>
            </Layout>
            <Redirect to='/' />
          </Switch>
        )
      } else {
        routes = (
          <Switch>
            <Layout>
              <Switch>
                <Route path="/traffic" component={TrafficDataPage} />
                <Route path="/login" component={LoginPage} />
                <Route path="/logout" component={Logout} />
                <Route path="/node" component={NodePage} />
                <Route path="/road" component={RoadPage} />
                <Route path="/:sevStat" component={HomePage} />
                <Route path="/" exact component={HomePage} />
              </Switch>
            </Layout>
            <Redirect to='/' />
          </Switch>
        )
      }
    }
    return (
      <div>
        {routes}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null,
    adminStatus: state.auth.status,
    path: state.auth.authRedirectPath
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: (path) => dispatch(actions.authCheckState(path))
  }
}

export default Snackbar(withRouter(connect(mapStateToProps, mapDispatchToProps)(App)), axios);
