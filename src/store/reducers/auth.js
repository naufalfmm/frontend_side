import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    token: null,
    loading: false,
    username: null,
    status: null,
    authRedirectPath: '/'
}

const reducer = (state=initialState, action) => {
    console.log(action.type, action)
    switch (action.type) {
        case actionTypes.AUTH_START:
            return updateObject(state, {loading: true})
        case actionTypes.AUTH_SUCCESS:
            return updateObject(state, {
                token: action.token,
                username: action.username,
                status: action.status,
                loading: false
            })
        case actionTypes.AUTH_FAIL:
            return updateObject(state, {
                loading: false
            })
        case actionTypes.AUTH_LOGOUT:
            return updateObject(state, {token: null, username: null, type: null, loading: false, authRedirectPath: '/'})
        case actionTypes.SET_AUTH_REDIRECT_PATH:
            return updateObject(state, { authRedirectPath: action.path })
        default:
            return state
    }
}

export default reducer