import axios from 'axios';
import jwt_decode from 'jwt-decode';

import * as actionTypes from './actionTypes';

import { setToken } from '../../axios-ins';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    }
}

export const authSuccess = (token, username, status) => {
    setToken(token)
    console.log("authSuccess", token, username, status)
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token,
        username: username,
        status: status
    }
}

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL
    }
}

export const logout = () => {
    localStorage.removeItem('token')
    localStorage.removeItem('expirationDate')
    localStorage.removeItem('adminType')
    localStorage.removeItem('username')
    return {
        type: actionTypes.AUTH_LOGOUT
    }
}

export const checkAuthTimeout = (expirationTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout())
        }, expirationTime * 1000)
    }
}

export const auth = (email, password) => {
    return dispatch => {
        dispatch(authStart())
        const authData = {
            username: email,
            password: password
        }
        console.log("Login")
        axios.post('http://localhost:7777/api/admin/login', authData)
            .then(res => {
                console.log(res.data, res.data.token)
                let decoded = jwt_decode(res.data.token)
                localStorage.setItem('token', res.data.token)
                localStorage.setItem('expirationDate', decoded.exp)
                localStorage.setItem('adminType', decoded.status)
                localStorage.setItem('username', decoded.username)
                dispatch(authSuccess(res.data.token, decoded.username, decoded.status))
            })
            .catch(() => {
                dispatch(authFail())
            })
    }
}

export const setAuthRedirectPath = (path) => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH,
        path: path
    }
}

export const authCheckState = (path) => {
    return dispatch => {
        console.log("authCheckState")
        const token = localStorage.getItem('token')
        if (!token) {
            dispatch(logout())
        } else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'))
            if (expirationDate <= new Date()) {
                dispatch(logout())
            } else {
                const username = localStorage.getItem('username')
                const type = localStorage.getItem('adminType')
                dispatch(authSuccess(token, username, type))
                if (path !== null) {
                    dispatch(setAuthRedirectPath(path))
                }
            }
        }
    }
}